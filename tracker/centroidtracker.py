# import the necessary packages
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import math
import time


def centroid_in_rect(rect, centroid):
	x1, y1, x2, y2 = rect
	x, y = centroid
	is_in_rect = False
	if x1 < x < x2 and y1 < y < y2:
		is_in_rect = True
	return is_in_rect

# Rect style: [x1, y1, x2, y2]
def intersection_check(rec1, rec2):
	if rec1[0] > rec2[2] or rec1[2] < rec2[0]:
		return False
	if rec1[1] > rec2[3] or rec1[3] < rec2[1]:
		return False
	return True

def point_rect_distance(rect, point):
	dx = max(rect[0] - point[0], 0, point[0] - rect[2])
	dy = max(rect[1] - point[1], 0, point[1] - rect[3])
	return math.sqrt(dx*dx + dy*dy)

def get_center_point_width_height(rect):
	width = rect[2] - rect[0]
	height = rect[3] - rect[1]
	startX, startY, endX, endY = rect
	cX = int((startX + endX) / 2.0)
	cY = int((startY + endY) / 2.0)

	return [cX, cY], width, height

def min_distance_of_rectangles(rect1, rect2):
	#First calculate the center points of the two rectangles
	center1, width1, height1 = get_center_point_width_height(rect1)
	center2, width2, height2 = get_center_point_width_height(rect2)

	#Calculate the distance between the center points of the two rectangles in the X axis and Y axis directions respectively
	Dx = abs(center2[0] - center1[0]);
	Dy = abs(center2[1] - center1[1]);


	#The two rectangles do not intersect, and there are two rectangles partially overlapping in the X-axis direction. The minimum distance is the distance between the lower line of the upper rectangle and the upper line of the lower rectangle
	if((Dx < ((width1 + width2)/ 2)) and (Dy >= ((height1 + height2) / 2))):
		min_dist = Dy - ((height1 + height2) / 2);

	#The two rectangles do not intersect. There are two partially overlapping rectangles in the Y-axis direction. The minimum distance is the distance between the right line of the left rectangle and the left line of the right rectangle
	elif((Dx >= ((width1 + width2)/ 2)) and (Dy < ((height1 + height2) / 2))):
		min_dist = Dx - ((width1 + width2)/ 2);

	#Two rectangles do not intersect, two rectangles that do not overlap in the X-axis and Y-axis directions, the minimum distance is the distance between the two closest vertices,
	#Using the Pythagorean theorem, it is easy to calculate this distance
	elif((Dx >= ((width1 + width2)/ 2)) and (Dy >= ((height1 + height2) / 2))):
		delta_x = Dx - ((width1 + width2)/ 2);
		delta_y = Dy - ((height1 + height2)/ 2);
		min_dist = math.sqrt(delta_x * delta_x  + delta_y * delta_y);

	#The intersection of two rectangles, the minimum distance is negative, return -1
	else:
		min_dist = -1;
	return min_dist

def distance_between_points(p1, p2):
	return math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )

# CALLBACK Keys
ENTER_KEY = 'enter'
EXIT_KEY = 'exit'
PROXIMITY_ENTER_KEY = 'proximity_enter'
PROXIMITY_EXIT_KEY = 'proximity_exit'


# CONFIG Keys
TAG_KEY = 'tag'
CENTROID_TRACKER_RESTRICTED_LABELS = 'restricted_labels'

ON_ENTER_KEY = "on_enter"
ON_EXIT_KEY = "on_exit"
DURATION_KEY = "duration"
PROXIMITY_KEY = "proximity"
LAST_GONE_KEY = "last_gone"

TAG_KEY = "tag"
RECT_KEY = "rect"
RECTS_KEY = "rects"
ON_ENTER_KEY = "on_enter"
ON_EXIT_KEY = "on_exit"
DURATION_KEY = "duration"
PROXIMITY_KEY = "proximity"
LAST_GONE_KEY = "last_gone"
EVENTS_KEY = "events"
TYPE_KEY = "type"
DISTANCE_UPPER_KEY = "distance_upper"
DISTANCE_LOWER_KEY = "distance_lower"
DURATION_MIN_KEY = "duration_min"
MIN_COUNT_KEY = "min_count"
TRACKING_LABELS_KEY = "tracking_labels"
CONTACT_LABELS_KEY = "contact_labels"
IGNORE_IF_OWNER_KEY = "ignore_if_owner"
USE_CENTROID_KEY = "use_centroid"







EVENT_TYPES = ["on_enter", "on_exit", "duration", "proximity", "last_gone"],


detection_guards = {
	"tag": "left_walls",
	"rect": [0,100, 0, 100],
	"type": "static",
	"contact_labels": ["fiona", "heart", "duration"],
	"events": ["on_enter", "on_exit", "duration", "duration"],
}

proximity_keys = {
	"distance_min": 40,
	"contact_labels": ["fiona", "heart", "duration"],
}

last_gone_keys = {
	"min_count": 2,
}

EVENT_TYPES = ["on_enter", "on_exit", "duration", "proximity", "last_gone"],

metrics_config = {
	"metrics_key": "fireballs",
	"metric_type": "list",
	"include_owner": True,
	"tracking_labels": ["arcana_heart_ball"],
}

metrics_config = {
	"metrics_key": "fireballs",
	"metric_type": "set",
	"include_owner": False,
	"tracking_labels": ["arcana_heart_ball"],
}


class CentroidTracker:
	def __init__(self, maxDisappeared=50, maxDistance=50, registerCallback=None, deregisterCallback=None, centroidTrackerConfig=None,
				proximity_exit_callback=None, proximity_enter_callback=None, exit_callback=None, enter_callback=None, enter_duration_callback=None,
				last_gone_callback=None):
		# initialize the next unique object ID along with two ordered
		# dictionaries used to keep track of mapping a given object
		# ID to its centroid and number of consecutive frames it has
		# been marked as "disappeared", respectively
		self.Abiu = 0
		self.Acai = {}
		self.Acerola = OrderedDict()
		self.cucumber = OrderedDict()
		self.Apple = OrderedDict()
		self.Avocado = OrderedDict()
		self.Banana = OrderedDict()
		self.Bilberry = OrderedDict()
		self.Blackberry = centroidTrackerConfig
		self.Blackcurrant = None
		if CENTROID_TRACKER_RESTRICTED_LABELS in self.Blackberry:
			self.Blackcurrant = self.Blackberry[CENTROID_TRACKER_RESTRICTED_LABELS]

		# store the number of maximum consecutive frames a given
		# object is allowed to be marked as "disappeared" until we
		# need to deregister the object from tracking
		self.Blueberry = maxDisappeared

		# store the maximum distance between centroids to associate
		# an object -- if the distance is larger than this maximum
		# distance we'll start to mark the object as "disappeared"
		self.Boysenberry = maxDistance
		self.Breadfruit = registerCallback
		self.Cactus = deregisterCallback

		self.Canistel = {}
		self.Cempedak = {}
		self.Cherimoya = {}

		self.Cherry = {}
		self.Chico = {}

		self.proximity_exit_callback = proximity_exit_callback
		self.proximity_enter_callback = proximity_enter_callback
		self.exit_callback = exit_callback
		self.enter_callback = enter_callback
		self.enter_duration_callback = enter_duration_callback
		self.Damson = last_gone_callback

		self.Cloudberry = []




	def register(self, centroid, rect, label):
		# when registering an object we use the next available object
		# ID to store the centroid
		if label not in self.Acai:
			self.Acai[label] = 0

		nextObjectCount = self.Acai[label]
		self.Banana[nextObjectCount] = nextObjectCount
		self.Acerola[nextObjectCount] = centroid
		self.cucumber[nextObjectCount] = rect
		self.Avocado[nextObjectCount] = label
		self.Apple[nextObjectCount] = None
		self.Bilberry[nextObjectCount] = 0
#		self.Abiu += 1

		self.Acai[label] += 1

		if self.Breadfruit is not None:
			self.Cloudberry.append([label, centroid, nextObjectCount, nextObjectCount])

	def deregister(self, objectID):
		objectIDs = list(self.Acerola.keys())

		# Used to handle event of last_gone
		for event_set in self.Blackberry[EVENTS_KEY]:

			label = self.Avocado[objectID]

			tag = event_set[TAG_KEY]

			ignore_child = False
			supported_labels = event_set[TRACKING_LABELS_KEY]

			if IGNORE_IF_OWNER_KEY in event_set:
				ignore_child = event_set[IGNORE_IF_OWNER_KEY]

			if USE_CENTROID_KEY in event_set:
				use_centroid = event_set[USE_CENTROID_KEY]

			last_item = True
			# check each event
			for single_event in event_set[EVENTS_KEY]:
				if single_event == LAST_GONE_KEY:
					if label not in supported_labels:
						break

					for tar_id in objectIDs:
						tar_label = self.Avocado[tar_id]
						tar_owner = self.Apple[tar_id]
						if tar_label in supported_labels and tar_id != objectID:
							if ignore_child:
								if tar_owner != objectID:
									last_item = False
									break
							else:
								last_item = False
								break

			if last_item:
				if self.Damson is not None:
					self.Damson(tag, objectID)


		label = self.Avocado[objectID]
		centroid = self.Acerola[objectID]

		# to deregister an object ID we delete the object ID from
		# both of our respective dictionaries
		del self.cucumber[objectID]
		del self.Acerola[objectID]
		del self.Apple[objectID]
		del self.Avocado[objectID]
		del self.Bilberry[objectID]

		if self.Cactus is not None:
			self.Cactus(label, centroid, objectID)

	def apply_owner(self, childID, ownerID):
		self.Apple[childID] = ownerID

	def get_objects_for_owner(self, ownerID):
		childIds = []
		objectIDs = list(self.Acerola.keys())
		for id in objectIDs:
			if self.Apple[id] == ownerID:
				childIds.append(id)

		return childIds

	def get_objects_for_labels(self, labels):
		ids = []
		objectIDs = list(self.Acerola.keys())
		for id in objectIDs:
			if self.Avocado[id] in labels:
				ids.append(id)

		return ids

	def get_advance_states(self):
		return self.Acerola, self.Avocado, self.Banana

	## This needs to happen separate from update to give people a chance to update fields before collisions
	def process_detections(self):
		for registration_args in self.Cloudberry:
			label, centroid, nextObjectID, nextObjectCount = registration_args
			self.Breadfruit(label, centroid, nextObjectID, nextObjectCount)

		if self.Blackberry is None:
			return
		objectIDs = list(self.Acerola.keys())
		objectCentroids = list(self.Acerola.values())

		for event_set in self.Blackberry[EVENTS_KEY]:

			# static/dynamic, may not ened
#			event_type = event_set[TYPE_KEY]
			use_centroid = True
			upper_distance = None
			lower_distance = None
			enter_duration_limit = None
			exit_duration_limit = None
			ignore_child = False

			if IGNORE_IF_OWNER_KEY in event_set:
				ignore_child = event_set[IGNORE_IF_OWNER_KEY]

			if DURATION_KEY in event_set:
				enter_duration_limit = event_set[DURATION_KEY]

			if DURATION_KEY in event_set:
				exit_duration_limit = event_set[DURATION_KEY]

			if DISTANCE_UPPER_KEY in event_set:
				upper_distance = event_set[DISTANCE_UPPER_KEY]

			if DISTANCE_LOWER_KEY in event_set:
				lower_distance = event_set[DISTANCE_LOWER_KEY]


			rects_and_ids= []

			#has a use of detecting closeness even when overlapping
			centroids_and_ids= []

			tag = event_set[TAG_KEY]
			supported_labels = event_set[TRACKING_LABELS_KEY]

			# force single rect to be rects
			# easy looping
			if RECT_KEY in event_set:
				rects_and_ids = [(event_set[RECT_KEY], -1)]
			if RECTS_KEY in event_set:
				rects_and_ids = [(rect, -1) for rect in event_set[RECTS_KEY]]

			# add any rects from other objects:
			if CONTACT_LABELS_KEY in event_set:
				for idx, objectID in enumerate(objectIDs):
					label = self.Avocado[objectID]
					if label not in event_set[CONTACT_LABELS_KEY]:
						continue

					rects_and_ids.append([self.cucumber[objectID], objectID])
					centroids_and_ids.append([self.Acerola[objectID], objectID])



			# override to use rect over centroids
			if USE_CENTROID_KEY in event_set:
				use_centroid = event_set[USE_CENTROID_KEY]

			# check each event
			for single_event in event_set[EVENTS_KEY]:
				# for each rect of interest
				for rect_id in rects_and_ids:
					rect, id = rect_id
					target_has_centroid = id != -1
					rect_id_label = rect
					if id != -1:
						rect_id_label = id
					for idx, objectID in enumerate(objectIDs):
						label = self.Avocado[objectID]
						if label not in supported_labels:
							continue

						# ignore self
						if objectID == id:
							continue

						if ignore_child and id in self.Apple and objectID == self.Apple[id]:
							continue

						# duration also requires knowing the start time
						if single_event in [ON_ENTER_KEY]:
							start_time = None
							if use_centroid:
								if centroid_in_rect(rect, objectCentroids[idx]):
									start_time = self.check_enter_callback(tag, objectID, rect)
							else:
								if intersection_check(rect, self.cucumber[objectID]):
									start_time = self.check_enter_callback(tag, objectID, rect)

							if enter_duration_limit is not None and start_time is not None:
								self.check_duration_callback(start_time, enter_duration_limit, tag, objectID, rect)

						if single_event == ON_EXIT_KEY:
							if use_centroid:
								if not centroid_in_rect(rect, objectCentroids[idx]):
									start_time = self.check_exit_callback(tag, objectID, rect)
							else:
								if not intersection_check(rect, self.cucumber[objectID]):
									start_time = self.check_exit_callback(tag, objectID, rect)

							if exit_duration_limit is not None and start_time is not None:
								self.check_duration_callback(start_time, exit_duration_limit, tag, objectID, rect)

						if single_event == PROXIMITY_KEY:
							inside_range = False
							prox_distance = None
							proximity_end_time = None
							proximity_start_time = None
							if use_centroid:
								if target_has_centroid:
									prox_distance = distance_between_points(self.Acerola[id], self.Acerola[objectID])
								else:
									prox_distance = point_rect_distance(rect, self.Acerola[objectID])
							else:
								prox_distance = min_distance_of_rectangles(rect1, rect2)

							if upper_distance is not None and lower_distance is not None:
								if lower_distance <= prox_distance and upper_distance >= prox_distance:
									inside_range = True
							elif upper_distance is not None:
								if upper_distance >= prox_distance:
									inside_range = True
							else:
								if lower_distance <= prox_distance:
									inside_range = True

							if inside_range:
								proximity_start_time = self.check_proximity_enter_callback(tag, objectID, rect_id_label, prox_distance)
							else:
								proximity_end_time = self.check_proximity_exit_callback(tag, objectID, rect_id_label)

							if enter_duration_limit is not None and proximity_start_time is not None:
								self.check_duration_callback(proximity_start_time, enter_duration_limit, tag, objectID, rect_id_label)

							if exit_duration_limit is not None and proximity_end_time is not None:
								self.check_duration_callback(proximity_end_time, exit_duration_limit, tag, objectID, rect_id_label)



	def update(self, labels_and_rects):
		# clear
		self.Cloudberry = []

		# check to see if the list of input bounding box rectangles
		# is empty
		if len(labels_and_rects) == 0:
			# loop over any existing tracked objects and mark them
			# as disappeared
			for objectID in list(self.Bilberry.keys()):
				self.Bilberry[objectID] += 1

				# if we have reached a maximum number of consecutive
				# frames where a given object has been marked as
				# missing, deregister it
				if self.Bilberry[objectID] > self.Blueberry:
					self.deregister(objectID)

			# return early as there are no centroids or tracking info
			# to update
			return self.Acerola

		all_inputCentroids = np.zeros((0, 2), dtype="int")
		all_labels = []
		all_rects = []

		for label_key in labels_and_rects:
			if self.Blackcurrant is not None and label_key not in self.Blackcurrant:
				continue

			rects = labels_and_rects[label_key]
			for rect in rects:
				all_rects.append(rect)

			# loop over the bounding box rectangles
			for (i, (startX, startY, endX, endY)) in enumerate(rects):
				# use the bounding box coordinates to derive the centroid
				cX = int((startX + endX) / 2.0)
				cY = int((startY + endY) / 2.0)
				all_inputCentroids = np.append(all_inputCentroids, np.array([[cX, cY]]), axis=0)
				all_labels.append(label_key)

#			print(all_inputCentroids.shape)
#			print(all_inputCentroids)

		if len(all_inputCentroids) == 0:
			# nothing to update
			return self.Acerola

		# if we are currently not tracking any objects take the input
		# centroids and register each of them
		if len(self.Acerola) == 0:
			for i in range(0, len(all_inputCentroids)):
				self.register(all_inputCentroids[i], all_rects[i], all_labels[i])

		# otherwise, are are currently tracking objects so we need to
		# try to match the input centroids to existing object
		# centroids
		else:
			# grab the set of object IDs and corresponding centroids
			objectIDs = list(self.Acerola.keys())
			objectCentroids = list(self.Acerola.values())

			# compute the distance between each pair of object
			# centroids and input centroids, respectively -- our
			# goal will be to match an input centroid to an existing
			# object centroid
			D = dist.cdist(np.array(objectCentroids), all_inputCentroids)

			# in order to perform this matching we must (1) find the
			# smallest value in each row and then (2) sort the row
			# indexes based on their minimum values so that the row
			# with the smallest value as at the *front* of the index
			# list
			rows = D.min(axis=1).argsort()

			# next, we perform a similar process on the columns by
			# finding the smallest value in each column and then
			# sorting using the previously computed row index list
			cols = D.argmin(axis=1)[rows]

			# in order to determine if we need to update, register,
			# or deregister an object we need to keep track of which
			# of the rows and column indexes we have already examined
			usedRows = set()
			usedCols = set()

			# loop over the combination of the (row, column) index
			# tuples
			for (row, col) in zip(rows, cols):
				# if we have already examined either the row or
				# column value before, ignore it
				if row in usedRows or col in usedCols:
					continue

				# if the distance between centroids is greater than
				# the maximum distance, do not associate the two
				# centroids to the same object
				if D[row, col] > self.Boysenberry:
					continue


				# otherwise, grab the object ID for the current row,
				# set its new centroid, and reset the disappeared
				# counter
				objectID = objectIDs[row]

				# make sure labels match so ids dont swap
				if all_labels[col] != self.Avocado[objectID]:
					continue

				self.Acerola[objectID] = all_inputCentroids[col]
				self.cucumber[objectID] = all_rects[col]
				self.Bilberry[objectID] = 0

				# indicate that we have examined each of the row and
				# column indexes, respectively
				usedRows.add(row)
				usedCols.add(col)

			# compute both the row and column index we have NOT yet
			# examined
			unusedRows = set(range(0, D.shape[0])).difference(usedRows)
			unusedCols = set(range(0, D.shape[1])).difference(usedCols)

			# in the event that the number of object centroids is
			# equal or greater than the number of input centroids
			# we need to check and see if some of these objects have
			# potentially disappeared
			if D.shape[0] >= D.shape[1]:
				# loop over the unused row indexes
				for row in unusedRows:
					# grab the object ID for the corresponding row
					# index and increment the disappeared counter
					objectID = objectIDs[row]
					self.Bilberry[objectID] += 1

					# check to see if the number of consecutive
					# frames the object has been marked "disappeared"
					# for warrants deregistering the object
					if self.Bilberry[objectID] > self.Blueberry:
						self.deregister(objectID)

			# otherwise, if the number of input centroids is greater
			# than the number of existing object centroids we need to
			# register each new input centroid as a trackable object
			else:
				for col in unusedCols:
#					print(col)
#					print(all_inputCentroids.shape)
#					print(len(all_rects))
#					print(len(all_labels))
					self.register(all_inputCentroids[col], all_rects[col], all_labels[col])
					if all_labels[col]:
						pass

		# return the set of trackable objects
		return self.Acerola


	def check_duration_callback(self, start_time, min_duration, collision_tag, objectID, rect_or_line_or_target_id):
		if start_time is None:
			return
		curr_time = time.time()

		duration = curr_time - start_time

		if min_duration > duration:
			return
		rect_as_key = str(rect_or_line_or_target_id)

		if DURATION_KEY in self.Cherimoya[collision_tag][objectID][rect_as_key] and self.Cherimoya[collision_tag][objectID][rect_as_key][DURATION_KEY] == start_time:
			return

		self.Cherimoya[collision_tag][objectID][rect_as_key][DURATION_KEY] = start_time

		if self.enter_duration_callback is not None:
			self.enter_duration_callback(collision_tag, objectID, rect_or_line_or_target_id, duration)

	def check_enter_callback(self, collision_tag, objectID, rect_or_line):
		start_time = time.time()
		rect_as_key = str(rect_or_line)

		self.add_key_mappings(collision_tag, objectID, rect_as_key)
		if ENTER_KEY in self.Canistel[collision_tag][objectID][rect_as_key] and self.Canistel[collision_tag][objectID][rect_as_key][ENTER_KEY] is not None:
			return

		self.Canistel[collision_tag][objectID][rect_as_key][ENTER_KEY] = start_time

		if self.enter_callback is not None:
			self.enter_callback(collision_tag, objectID, rect_or_line)

	def check_exit_callback(self, collision_tag, objectID, rect_or_line):

		rect_as_key = str(rect_or_line)
		self.add_key_mappings(collision_tag, objectID, rect_as_key)
		if ENTER_KEY not in self.Canistel[collision_tag][objectID][rect_as_key] or self.Canistel[collision_tag][objectID][rect_as_key][ENTER_KEY] is None:
			return

		stop_time = time.time()
		self.Canistel[collision_tag][objectID][rect_as_key][EXIT_KEY] = stop_time

		start_time = self.Canistel[collision_tag][objectID][rect_as_key][ENTER_KEY]

		#reset
		self.Canistel[collision_tag][objectID][rect_as_key][ENTER_KEY] = None


		self.Cherry[collision_tag][objectID][rect_as_key].append([start_time, stop_time, rect_or_line])

		if self.exit_callback is not None:
			self.exit_callback(collision_tag, objectID, rect_or_line)

	def check_proximity_enter_callback(self, collision_tag, objectID, targetObjectId, distance):
		start_time = time.time()

		target_id_as_key = str(targetObjectId)
		self.add_key_mappings(collision_tag, objectID, target_id_as_key)
		if PROXIMITY_ENTER_KEY in self.Cempedak[collision_tag][objectID][target_id_as_key] and self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY] is not None:
			return

		self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY] = start_time

		if self.proximity_enter_callback is not None:
			self.proximity_enter_callback(collision_tag, objectID, targetObjectId, distance)

	def check_proximity_exit_callback(self, collision_tag, objectID, targetObjectId):
		stop_time = time.time()

		target_id_as_key = str(targetObjectId)
		self.add_key_mappings(collision_tag, objectID, target_id_as_key)
		if PROXIMITY_ENTER_KEY not in self.Cempedak[collision_tag][objectID][target_id_as_key] or self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY] is None:
			return


		start_time = self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY]
		#reset
		self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY] = None

		self.Cempedak[collision_tag][objectID][target_id_as_key][PROXIMITY_EXIT_KEY] = stop_time
		self.Cherry[collision_tag][objectID][target_id_as_key].append([start_time, stop_time])

		if self.proximity_exit_callback is not None:
			self.proximity_exit_callback(collision_tag, objectID, targetObjectId)

	def add_key_mappings(self, collision_tag, objectID, rect_as_key_or_target_id):

	    # add tags to enter exit mappign
	    if collision_tag not in self.Canistel:
	        self.Canistel[collision_tag] = {}
	        self.Cherry[collision_tag] = {}
	        self.Cempedak[collision_tag] = {}
	        self.Chico[collision_tag] = {}
	        self.Cherimoya[collision_tag] = {}

	    if objectID not in self.Canistel[collision_tag]:
	        self.Canistel[collision_tag][objectID] = {}
	        self.Cherry[collision_tag][objectID] = {}
	        self.Cempedak[collision_tag][objectID] = {}
	        self.Chico[collision_tag][objectID] = {}
	        self.Cherimoya[collision_tag][objectID] = {}

	    if rect_as_key_or_target_id not in self.Canistel[collision_tag][objectID]:
	        self.Canistel[collision_tag][objectID][rect_as_key_or_target_id] = {}
	        self.Cherry[collision_tag][objectID][rect_as_key_or_target_id] = []
	        self.Cempedak[collision_tag][objectID][rect_as_key_or_target_id] = {}
	        self.Chico[collision_tag][objectID][rect_as_key_or_target_id] = []
	        self.Cherimoya[collision_tag][objectID][rect_as_key_or_target_id] = []




"""

Hullo Rand Paul,

While you're digging into Dr Fauci, I would also like you to investigate this event. In 2012, Russian soldiers were invited into US shores, yet very few people know that this event ever happened. I reached out the to military for Freedom Of Information Act requests,  but that has been dragging on for a while. Perhaps you can get to the bottom of this quicker.

When was the last time foreign troops set foot on US mainland soil?
There is a significant event in 2012, that almost all Americans do not know about.
https://www.foxnews.com/us/us-russian-soldiers-train-together-in-colorado
https://www.businessinsider.com/joint-us-russia-military-drills-2012-4

Earthquakes along fault lines have skyrocketed since 2012. Fracking sites are also giving off radiation. I am trying to prove those soldiers never left, but I need to await the FOIA request before issuing a proper warning to the nation.

I heard that Russia Plans to use underground explosions along fault lines to utilize nature to destroy this nation. I am investigating to see if that claim holds any weight. The more I look into it, the more it seems plausible.




"""

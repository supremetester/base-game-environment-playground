# upload_blob_images_parallel.py
# Python program to bulk upload jpg image files as blobs to azure storage
# Uses ThreadPool for faster parallel uploads!
# Uses latest python SDK() for Azure blob storage
# Requires python 3.6 or above
import os
from multiprocessing.pool import ThreadPool
from azure.storage.blob import BlobServiceClient, BlobClient
from azure.storage.blob import ContentSettings, ContainerClient

import json
azure_config = json.load(open('config/azure_config.json',))
runner_config = json.load(open('config/runner_config.json',))
#?sv=2020-02-10&ss=bfqt&srt=sco&sp=rwdlacuptfx&se=2021-09-16T02:00:57Z&st=2021-05-21T18:00:57Z&spr=https&sig=Th9gtbSFNYaTslPnUqjSc2qJDbMxvITigZ2as3O4axo%3D

# IMPORTANT: Replace connection string with your storage account connection string
# Usually starts with DefaultEndpointsProtocol=https;...
MY_CONNECTION_STRING = azure_config["AZURE_STORAGE_CONNECTION_STRING"]

# Replace with blob container
MY_IMAGE_CONTAINER = azure_config["AZURE_STORAGE_CONTAINER"]

# Replace with the local folder which contains the image files for upload
LOCAL_IMAGE_PATH = "data_upload"

class AzureBlobFileUploader:
  def __init__(self):
    print("Intializing AzureBlobFileUploader")

    # Initialize the connection to Azure storage account
    self.blob_service_client =  BlobServiceClient.from_connection_string(MY_CONNECTION_STRING)

    my_container = self.blob_service_client.get_container_client(MY_IMAGE_CONTAINER)

    try:
        # Create new container in the service
        my_container.create_container()

    except:
        pass

  def upload_all_images_in_folder(self):
    # Get all files with jpg extension and exclude directories
    all_file_names = [f for f in os.listdir(LOCAL_IMAGE_PATH)
                    if os.path.isfile(os.path.join(LOCAL_IMAGE_PATH, f)) and ".jpg" in f]

    result = self.run(all_file_names)
    print(result)

  def run(self,all_file_names):
    # Upload 10 files at a time!
    with ThreadPool(processes=int(10)) as pool:
      return pool.map(self.upload_image, all_file_names)

  def upload_image(self,file_name):
    # Create blob with same name as local file name
    blob_client = self.blob_service_client.get_blob_client(container=MY_IMAGE_CONTAINER,
                                                          blob=file_name)
    # Get full path to the file
    upload_file_path = os.path.join(LOCAL_IMAGE_PATH, file_name)

    # Create blob on storage
    # Overwrite if it already exists!
    image_content_setting = ContentSettings(content_type='image/jpeg')
    print(f"uploading file - {file_name}")
    with open(upload_file_path, "rb") as data:
      blob_client.upload_blob(data,overwrite=True,content_settings=image_content_setting)
    return file_name

# Initialize class and upload files
azure_blob_file_uploader = AzureBlobFileUploader()
azure_blob_file_uploader.upload_all_images_in_folder()


import os

import uuid

try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
from os import path
from process_controller_data import *



r = {}


def get_controller_parsed_data(controller_data, controller_index, timestamp, user_id, session_id):

    prev_controller, prev_timer = get_controller_for_session(user_id, session_id, controller_index)
    timestamp = int(timestamp)
    controller_src = controller_data.split(',')
    controller_src_ints = [float(x) for x in controller_src]
    # scale down Left_X, Left_Y, Right_X, Right_y
    controller_src_ints[0] = round(controller_src_ints[0] / 255, 6)
    controller_src_ints[1] = round(controller_src_ints[1] / 255, 6)
    controller_src_ints[2] = round(controller_src_ints[2] / 255, 6)
    controller_src_ints[3] = round(controller_src_ints[3] / 255, 6)

    curr_controller = get_controller_state(controller_src_ints)

    curr_timer, delta_timer = get_controller_time(curr_controller, prev_controller, prev_timer, timestamp)
    controller_str_rep = get_controller_string_rep(curr_controller, delta_timer)
    # remove trailing comma
    controller_str_rep = controller_str_rep.strip()[:-1]

    prev_controller = curr_controller
    prev_timer = curr_timer

    write_controller_for_session(user_id, session_id, prev_controller, prev_timer, controller_index)


    return controller_str_rep

def get_controller_for_session(user_id, session_id, controller_index):
    prev_controller, prev_timer = get_empty_controller_state(), get_empty_controller_state(True)

    prev_controller_key = "%s:%s:%d:prev_timer" % (user_id, session_id, controller_index)
    prev_timer_key = "%s:%s:%d:prev_controller" % (user_id, session_id, controller_index)

    stored_prev_controller = r.get(prev_controller_key)
    stored_prev_timer = r.get(prev_timer_key)

    if stored_prev_controller is None or stored_prev_timer is None:
        return prev_controller, prev_timer

    prev_controller_src = stored_prev_controller.split(',')
    prev_controller_src_ints = [float(x) for x in prev_controller_src]
    prev_controller = get_controller_state(prev_controller_src_ints)

    prev_time_src = stored_prev_timer.split(',')
    prev_time_src_ints = [float(x) for x in prev_time_src]
    prev_timer = get_controller_state(prev_time_src_ints)

    return prev_controller, prev_timer

def write_controller_for_session(user_id, session_id, prev_controller, prev_timer, controller_index):
    prev_cont_data = controller_state_to_ints(prev_controller)
    prev_time_data = controller_state_to_ints(prev_timer)

    prev_controller_key = "%s:%s:%d:prev_timer" % (user_id, session_id, controller_index)
    prev_timer_key = "%s:%s:%d:prev_controller" % (user_id, session_id, controller_index)

    r[prev_controller_key] = prev_cont_data
    r[prev_timer_key] = prev_time_data


def update_controller_for_session(user_id, session_id, controller_state, timestamp, controller_index):
    controller_delta_data = get_controller_parsed_data(controller_state, controller_index, timestamp, user_id, session_id)
    return controller_delta_data



def write_controller_to_csv(user_id, session_id, image_name, controller, timestamp, controller_index, controller_info_raw):
    # we need to strip timing from each press
    new_class = controller_info_raw.replace(',', '_')
    kifu_csv_row = '%s,%s,%d,%s,%s' % (image_name, timestamp, controller_index, controller, new_class)

    # store for csv
    controller_csv_key = "%s:%s:%d:controller_csv" % (user_id, session_id, controller_index)
    listy = r.get(controller_csv_key)
    if listy is None:
        listy = []
    listy.append(kifu_csv_row)
    r[controller_csv_key] = listy

# system specific yolo processing
def write_other_data_to_csv(user_id, session_id, image_filename, categories, values, data_csv_name):
    if len(categories) == 0:
        return

    categories.insert(0, 'ID')
    values.insert(0, image_filename)

    string_ints = [str(int) for int in values]

    categories_str = ','.join(categories)
    data_csv_row = ','.join(string_ints)
#    data_csv_row = ','.join(values)
    # store for csv
    extra_categories_names_key = "%s:%s:%s_categories_names" % (user_id, session_id, data_csv_name)
    r[extra_categories_names_key] = categories_str

    # store for csv
    data_csv_key = "%s:%s:%s" % (user_id, session_id, data_csv_name)
    listy = r.get(data_csv_key)
    if listy is None:
        listy = []
    listy.append(data_csv_row)
    r[data_csv_key] = listy


def get_controller_csv(user_id, session_id, controller_index):

    controller_csv_key = "%s:%s:%d:controller_csv" % (user_id, session_id, controller_index)
    listy = r.get(controller_csv_key)
    if listy is None:
        listy = []
    return listy

def get_categories_and_csv(user_id, session_id, data_csv_name):
    extra_categories_names_key = "%s:%s:%s_categories_names" % (user_id, session_id, data_csv_name)

    # store for csv
    data_csv_key = "%s:%s:%s" % (user_id, session_id, data_csv_name)

    listy = r.get(data_csv_key)
    if listy is None:
        listy = []

    return r.get(extra_categories_names_key), listy

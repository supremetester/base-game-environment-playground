import json
import requests


class ControllerManager():

    def __init__(self, user_id="1", session_id="21"):
        self.session_id = session_id
        self.user_id = user_id
        self.baseurl = 'http://localhost:6442/api/'

    def get_params(self):
        return {
            'session_id': self.session_id,
            'user_id': self.user_id
        }

    def fire_request(self, url, params):
        url = self.baseurl + url
        data_json = json.dumps(params)
        print('url:', url)
        headers = {'content-type': 'application/json'}

        response = requests.post(url, data=data_json, headers=headers)
        return response

    def update_controller_for_session(self, controller, timestamp):
        params = self.get_params()
        params['controller'] = controller
        params['timestamp'] = timestamp
        url = 'update_controller_for_session'
        return self.fire_request(url, params)

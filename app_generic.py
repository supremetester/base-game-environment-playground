# @Author: Dwivedi Chandan
# @Date:   2019-08-05T13:35:05+05:30
# @Email:  chandandwivedi795@gmail.com
# @Last modified by:   Dwivedi Chandan
# @Last modified time: 2019-08-07T11:52:45+05:30


# import the necessary packages
import numpy as np
import argparse
import time
import cv2
import os
#import binascii
import io
import json
from PIL import Image
import base64
#from flask_dynamo import Dynamo

import logging

import uuid

try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3


import json

from datetime import datetime

import pandas as pd
from redis_controller_manager import *

import os
DEBUG = bool(int(os.environ.get('DEBUG', 1)))
S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'generic-game-replay-recordings')
IGNORE_AWS = bool(int(os.environ.get('IGNORE_AWS', 1)))

ar = {}

import json
azure_config = json.load(open('config/azure_config.json',))


GAME_NAME = 'SWORD_WILDAREA'
GAME_FIELD_CSV_KEY = 'GAME_FIELD_CSV_KEY'
GAME_STATE_CSV_KEY = 'GAME_STATE_CSV_KEY'
USER_FIELD_CSV_KEY = 'USER_FIELD_CSV_KEY'

#CONTROLLER_HEADER = "id,timestamp,CONTROLLER_INDEX,LX,LX_time,LY,LY_time,RX,RX_time,RY,RY_time,UP,UP_time,DOWN,DOWN_time,L,L_time,R,R_time,A,A_time,B,B_time,X,X_time,Y,Y_time,LB,LB_time,RB,RB_time,LT,LT_time,RT,RT_time,New_class,New_encode"
CONTROLLER_HEADER = "id,timestamp,CONTROLLER_INDEX,LX,LX_time,LY,LY_time,RX,RX_time,RY,RY_time,UP,UP_time,DOWN,DOWN_time,L,L_time,R,R_time,A,A_time,B,B_time,X,X_time,Y,Y_time,LB,LB_time,RB,RB_time,LT,LT_time,RT,RT_time,New_class"
CONTROLLER_HEADER = CONTROLLER_HEADER.split(',')

system_categories_keys  = ['Direction', 'Area']
system_categories = {
    "direction": {
        "Name": "Direction",
        "default": 0
    },
    "area": {
        "Name": "Area",
        "default": "Area 1",
        "options": ["Area 1", "Area 2"]
    }
}

def process_stateless(user_id, session_id, game_name, image, controller_info_1, controller_info_2, timestamp, game_categories, game_values, user_categories, user_values):

    print('managers made')

    images_count_key = "%s:%s:images_count" % (user_id, session_id)
    images_count = r.get(images_count_key)
    if images_count is None:
        images_count = 0
    else:
        images_count = int(images_count)
    images_count += 1
    r[images_count_key] = images_count

    image_count_str = '%09d' % images_count
    image_filename = '%s_%s.jpg' % (session_id, image_count_str)

    testing_override_key = "%s:%s:testing_override" % (user_id, session_id)
    training_override_key = "%s:%s:training_override" % (user_id, session_id)

    if r.get(testing_override_key) == True:
        image_filename = 'TESTING_%s_%s.jpg' % (session_id, image_count_str)
    if r.get(training_override_key) == True:
        image_filename = 'TRAINING_%s_%s.jpg' % (session_id, image_count_str)

    # stash all image file names for zipping
    image_filenames_key = "%s:%s:image_filenames" % (user_id, session_id)
    listy = r.get(image_filenames_key)
    if listy is None:
        listy = []
    listy.append(image_filename)
    r[image_filenames_key] = listy

    controller_index = 0
    controller_data_transformed = update_controller_for_session(user_id, session_id, controller_info_1, timestamp, controller_index)
    write_controller_to_csv(user_id, session_id, image_filename, controller_data_transformed, timestamp, controller_index, controller_info_1)

    controller_index = 1
    controller_data_transformed = update_controller_for_session(user_id, session_id, controller_info_2, timestamp, controller_index)
    write_controller_to_csv(user_id, session_id, image_filename, controller_data_transformed, timestamp, controller_index, controller_info_2)

    gamestate_categories, gamestate_values = get_game_state_data()

#    print('game_values', game_values)
#    print('gamestate_values', gamestate_values)
#    print('user_values', user_values)
    # game required fields
    if len(game_categories) > 0 and len(game_values) > 0:
        write_other_data_to_csv(user_id, session_id, image_filename, game_categories, game_values, GAME_FIELD_CSV_KEY)

    # game state generated data
    if len(gamestate_categories) > 0 and len(gamestate_values) > 0:
        write_other_data_to_csv(user_id, session_id, image_filename, gamestate_categories, gamestate_values, GAME_STATE_CSV_KEY)


    # user fields
    if len(user_categories) > 0 and len(user_values) > 0:
        write_other_data_to_csv(user_id, session_id, image_filename, user_categories, user_values, USER_FIELD_CSV_KEY)

    use_crop_key = "%s:%s:use_crop" % (user_id, session_id)
    crop_rect_key = "%s:%s:crop_rect" % (user_id, session_id)
    crop_rect = None
    if r.get(use_crop_key) == 'True':
        crop_rect = json.loads(r.get(crop_rect_key))

    save_file_for_session(image, user_id, session_id, game_name, image_filename, crop_rect)


    return {"message":"Good!"}

def get_game_state_data():
    # dummy data to force csv creation
    return ["RUNNING"], [0]

def upload_csv_file_to_s3(user_id, session_id, csv_path):
    print('uploading:', csv_path)
    if IGNORE_AWS:
        print('Skipping S3 Upload')
        return
    filename_only = csv_path.split('/')[-1]
    upload_object_name = "%s/%s/unpacked/%s" % (user_id, session_id, filename_only)
    # upload zip to s3
    upload_file(csv_path, S3_BUCKET_NAME, upload_object_name)


def wrapup_session(user_id, session_id, game_name, zip_filename):
#    csv_dir = '~/local_csv_savings/%s/%s/' % (user_id, session_id)
#    csv_dir = '/tmp/local_csv_savings/'
    csv_dir = 'local_csv_savings/%s/%s/' % (game_name, session_id)
#    os.makedirs(csv_dir)
    if not os.path.exists(csv_dir):
        os.makedirs(csv_dir)

    csv_filenames = []
    csv_data_for_local_storage = {}

    # Get controller 1 csv
    controller_index = 0
    controller_csv_key = "%s:%s:%d:controller_csv" % (user_id, session_id, controller_index)
    controller_csv_filename = "%s_%d_controller.csv" % (session_id, controller_index)
    csv_filenames.append(controller_csv_filename)
    controller_data_raw = get_controller_csv(user_id, session_id, controller_index)
#    df = pd.DataFrame(controller_data_raw)
    df = pd.read_csv(io.StringIO('\n'.join(controller_data_raw)), names=CONTROLLER_HEADER)
    csv_data_for_local_storage[controller_csv_filename] = {}
    csv_data_for_local_storage[controller_csv_filename]['data'] = '\n'.join(controller_data_raw)
    csv_data_for_local_storage[controller_csv_filename]['header'] = CONTROLLER_HEADER

    """
    #Handle shift
    last_image = df.iat[-1,-1]
    print('last image is', last_image)

    df.iloc[:,-1] = df.iloc[:,-1].shift(-1)
    df = df[:-1]
    df['New_class'].iloc[[0,-1]] = last_image
    """

    df.to_csv(csv_dir + controller_csv_filename, index=False)
    csv_path = csv_dir + controller_csv_filename

    num_frames = int(df.size)

    # Get controller 2 csv
    controller_index = 1
    controller_csv_key = "%s:%s:%d:controller_csv" % (user_id, session_id, controller_index)
    controller_csv_filename = "%s_%d_controller.csv" % (session_id, controller_index)
    csv_filenames.append(controller_csv_filename)
    controller_data_raw = get_controller_csv(user_id, session_id, controller_index)
#    df = pd.DataFrame(controller_data_raw)
    df = pd.read_csv(io.StringIO('\n'.join(controller_data_raw)), names=CONTROLLER_HEADER)
    csv_data_for_local_storage[controller_csv_filename] = {}
    csv_data_for_local_storage[controller_csv_filename]['data'] = '\n'.join(controller_data_raw)
    csv_data_for_local_storage[controller_csv_filename]['header'] = CONTROLLER_HEADER

    """
    #Handle shift
    last_image = df.iat[-1,-1]
    print('last image is', last_image)

    df.iloc[:,-1] = df.iloc[:,-1].shift(-1)
    df = df[:-1]
    df['New_class'].iloc[[0,-1]] = last_image
    """


    df.to_csv(csv_dir + controller_csv_filename, index=False)
    csv_path = csv_dir + controller_csv_filename

    # get game extras csv
    categories, csv_data_raw = get_categories_and_csv(user_id, session_id, GAME_FIELD_CSV_KEY)
    csv_filename = "%s_%s.csv" % (session_id, GAME_FIELD_CSV_KEY)
    print('ddd categories', categories)
    print('ddd csv_data_raw', csv_data_raw)
    if categories is not None and len(csv_data_raw) > 0:
        csv_filenames.append(csv_filename)
#        df = pd.DataFrame(csv_data_raw)
        categories_headers = categories.split(',')
        df = pd.read_csv(io.StringIO('\n'.join(csv_data_raw)), names=categories_headers)
        csv_data_for_local_storage[csv_filename] = {}
        csv_data_for_local_storage[csv_filename]['data'] = '\n'.join(controller_data_raw)
        csv_data_for_local_storage[csv_filename]['header'] = CONTROLLER_HEADER
        df.to_csv(csv_dir + csv_filename, index=False)
        csv_path = csv_dir + csv_filename

    # get user defined csv
    categories, csv_data_raw = get_categories_and_csv(user_id, session_id, USER_FIELD_CSV_KEY)
    print('ddd categories', categories)
    print('ddd csv_data_raw', csv_data_raw)
    csv_filename = "%s_%s.csv" % (session_id, USER_FIELD_CSV_KEY)
    if categories is not None and len(csv_data_raw) > 0:
        csv_filenames.append(csv_filename)
#        df = pd.DataFrame(csv_data_raw)
        categories_headers = categories.split(',')
        df = pd.read_csv(io.StringIO('\n'.join(csv_data_raw)), names=categories_headers)
        csv_data_for_local_storage[csv_filename] = {}
        csv_data_for_local_storage[csv_filename]['data'] = '\n'.join(controller_data_raw)
        csv_data_for_local_storage[csv_filename]['header'] = CONTROLLER_HEADER
        df.to_csv(csv_dir + csv_filename, index=False)
        csv_path = csv_dir + csv_filename

    # store locally
    save_csv_file_for_local_session(user_id, session_id, csv_data_for_local_storage)

    package_training_data_for_session(user_id, session_id, zip_filename)


# route http posts to this method
def start_session(user_id, crop_rect, testing_override, training_override):
    # load our input image and grab its spatial dimensions
    #image = cv2.imread("./test1.jpg")
    session_id = str(uuid.uuid4())

    testing_override_key = "%s:%s:testing_override" % (user_id, session_id)
    training_override_key = "%s:%s:training_override" % (user_id, session_id)

    r[testing_override_key] = testing_override
    r[training_override_key] = training_override

    use_crop_key = "%s:%s:use_crop" % (user_id, session_id)
    crop_rect_key = "%s:%s:crop_rect" % (user_id, session_id)
    x1 = 0
    y1 = 0
    x2 = 1280
    y2 = 720

    is_valid = False
    if crop_rect is not None:
        raw_x1, raw_y1, raw_x2, raw_y2 = crop_rect
        raw_x1 = int(raw_x1)
        raw_y1 = int(raw_y1)
        raw_x2 = int(raw_x2)
        raw_y2 = int(raw_y2)
        is_valid = True
        if raw_x2 < raw_x1 or raw_x2 > 1280 or raw_x1 < 0:
            is_valid = False

        if raw_y2 < raw_y1 or raw_y2 > 720 or raw_y1 < 0:
            is_valid = False
        if is_valid:
            x1 = raw_x1
            y1 = raw_y1
            x2 = raw_x2
            y2 = raw_y2

    if is_valid:
        r[use_crop_key] = str(True)
    else:
        r[use_crop_key] = str(False)

    r[crop_rect_key] = str([x1, y1, x2, y2])
    print('rect is:',str([x1, y1, x2, y2]))

    print('start_session')
    # Starting/Stopping sessions

    return session_id


# route http posts to this method
def end_session(user_id, game_name, session_id, tags, description):
    # load our input image and grab its spatial dimensions
    #image = cv2.imread("./test1.jpg")

    # current date and time
    now = datetime.now()

    timestamp = datetime.timestamp(now)
    zip_filename = '%s_%d_%s' % (GAME_NAME, timestamp, session_id)

    wrapup_session(user_id, session_id, game_name, zip_filename)

# route http posts to this method
def required_game_fields():
    print(system_categories)
    return Response(response=json.dumps(system_categories), status=200,mimetype="application/json")

# route http posts to this method
def get_session_records():
    # Starting/Stopping sessions
    print('get_session_records')
    req_json = request.get_json()
    user_id = req_json["user_id"]
    session_id = None


    session_manager = SessionManager(user_id, session_id)
    if IGNORE_AWS:
        records = {}
    else:
        records = session_manager.get_sessions_for_user()
    return Response(response=json.dumps(records), status=200,mimetype="application/json")

# route http posts to this method
def predict_image():
    # load our input image and grab its spatial dimensions
    #image = cv2.imread("./test1.jpg")
    req_json = request.get_json()
    user_id = req_json["user_id"]
    session_id = req_json["session_id"]
    controller = req_json["controller"]

    img = request.files["image"].read()
    img = Image.open(io.BytesIO(img))
    npimg=np.array(img)
    image=npimg.copy()
    image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
    print('image made just fine')
    state_dump=process_stateless(user_id, session_id, image, controller)
    print(state_dump)
    return Response(response=json.dumps(state_dump), status=200,mimetype="application/json")

############ BEGIN FOR LOCAL CONTAINER STORAGE ONLY ###################
#local docker usage does not need the upload/download from AWS

# route http posts to this method
from azure.storage.blob import BlobServiceClient, BlobClient
from azure.storage.blob import ContentSettings, ContainerClient
import os
import zipfile

MY_CONNECTION_STRING = azure_config["AZURE_STORAGE_CONNECTION_STRING"]

# Replace with blob container name
MY_BLOB_CONTAINER = 'delete-me-test-%s' % str(uuid.uuid4())
MY_BLOB_CONTAINER = azure_config["AZURE_STORAGE_CONTAINER_BEHAVIOR"]




def save_file_for_session(image, user_id, session_id, game_name, frame_filename, crop_rect):

    FOLDER_NAME = '/tmp/workspaces/%s_%s' % (user_id, session_id)
    IMAGES_DIR = '%s/images' % (FOLDER_NAME)
    CROPPED_DIR = '%s/cropped_images' % (FOLDER_NAME)
    cropped_regex = "cropped_"


    base_dir = 'session_images/%s/%s' % (game_name, session_id)
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    if not os.path.exists(IMAGES_DIR):
        os.makedirs(IMAGES_DIR)

    if not os.path.exists(CROPPED_DIR):
        os.makedirs(CROPPED_DIR)

    filepath = '%s/%s' % (IMAGES_DIR, frame_filename)
    cv2.imwrite(filepath, image, [cv2.IMWRITE_JPEG_QUALITY, 85])

    if crop_rect is not None:
        x1, y1, x2, y2 = crop_rect
        cropped_image = image[y1:y2, x1:x2]
        filepath = '%s/cropped_%s' % (CROPPED_DIR, frame_filename)
        cv2.imwrite(filepath, cropped_image, [cv2.IMWRITE_JPEG_QUALITY, 85])

    print('image saved: ', frame_filename)

# route http posts to this method
def save_csv_file_for_local_session(user_id, session_id, bulk_csv_data):

    FOLDER_NAME = '/tmp/workspaces/%s_%s' % (user_id, session_id)

    if not os.path.exists(FOLDER_NAME):
        os.makedirs(FOLDER_NAME)

    for csv_filename in bulk_csv_data:
        csv_data = bulk_csv_data[csv_filename]
        df = pd.read_csv(io.StringIO(csv_data['data']), names=csv_data['header'])

        df.to_csv('%s/%s' % (FOLDER_NAME, csv_filename), index=False)

    print("Saving csv complete")

############ END FOR LOCAL CONTAINER STORAGE ONLY ###################





def upload_to_azure(zip_path, zip_filename):
    print('Upload to azure begin')
    blob_service_client =  BlobServiceClient.from_connection_string(MY_CONNECTION_STRING)
    my_container = blob_service_client.get_container_client(MY_BLOB_CONTAINER)

    try:
        # Create new container in the service
        my_container.create_container()

    except:
        pass

    # Create a blob client using the local file name as the name for the blob
    blob_client = blob_service_client.get_blob_client(container=MY_BLOB_CONTAINER, blob=zip_filename)
    with open(zip_path, "rb") as data:
        blob_client.upload_blob(data)

    print('Upload to azure complete')

# route http posts to this method
def package_training_data_for_session(user_id, session_id, zip_filename):
    resp = {}

    dist_name = "%s/%s/unpacked" % (user_id, session_id)
    zip_path = copy_dataset_to_workspace(user_id, session_id, zip_filename)
    zip_filename = zip_filename + '.zip'

    upload_to_azure(zip_path, zip_filename)


import shutil
def copy_dataset_to_workspace(user_id, session_id, zip_filename):
    folder_name = '/tmp/workspaces/%s_%s' % (user_id, session_id)
    images_dir = '%s/images/' % (folder_name)
    zip_dir = '/tmp/results/'

    crop_json = {
        'trim_from_start': 0,
        'trim_from_end':0
    }
    crop_str = json.dumps(crop_json)
    crop_filename  = '%s_crop.json' % session_id
    crop_file = open(folder_name + '/' + crop_filename, 'w')
    crop_file.write(crop_str)
    crop_file.close()

    shutil.make_archive(zip_dir + zip_filename, 'zip', folder_name)

    return zip_dir + zip_filename + '.zip'

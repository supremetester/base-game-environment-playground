from navigator.crimson_navigator import *
from poke_state_common import *
import cv2
from yolo_v3_examples.yolov3_manager import *

TEST_ENUM_MAPPINGS = {
    'BATTLE_STATES': BATTLE_STATES,
    'BATTLE_SELECTABLES': BATTLE_SELECTABLES,
    'BATTLE_SUBMENU_SELECTABLES': BATTLE_SUBMENU_SELECTABLES,
    'NETWORK_BATTLE_SUBMENU_SELECTABLES': NETWORK_BATTLE_SUBMENU_SELECTABLES,


    'STANDBY_CONSTRUCT_TEAM_INFO': NavigationAction.STANDBY_CONSTRUCT_TEAM_INFO,
    'STANDBY_ADD_FIRST_POKEMON_INFO': NavigationAction.STANDBY_ADD_FIRST_POKEMON_INFO,
    'STANDBY_ADD_SECOND_POKEMON_INFO': NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO,
    'STANDBY_ADD_THIRD_POKEMON_INFO': NavigationAction.STANDBY_ADD_THIRD_POKEMON_INFO,
    'STANDBY_ADD_FOURTH_POKEMON_INFO': NavigationAction.STANDBY_ADD_FOURTH_POKEMON_INFO,
    'STANDBY_ADD_FIFTH_POKEMON_INFO': NavigationAction.STANDBY_ADD_FIFTH_POKEMON_INFO,
    'STANDBY_ADD_SIXTH_POKEMON_INFO': NavigationAction.STANDBY_ADD_SIXTH_POKEMON_INFO,
    'STANDBY_COUNT_ACTIVE_POKEMON': NavigationAction.STANDBY_COUNT_ACTIVE_POKEMON,
    'STANDBY_CHECK_NEXT_ACTIVE': NavigationAction.STANDBY_CHECK_NEXT_ACTIVE,
    'STANDBY_ATTACK_SLOT_1': NavigationAction.STANDBY_ATTACK_SLOT_1,
    'STANDBY_ATTACK_SLOT_2': NavigationAction.STANDBY_ATTACK_SLOT_2,
    'STANDBY_ATTACK_SLOT_3': NavigationAction.STANDBY_ATTACK_SLOT_3,
    'STANDBY_ATTACK_SLOT_4': NavigationAction.STANDBY_ATTACK_SLOT_4,
    'STANDBY_PEEK_TEAM_INFO': NavigationAction.STANDBY_PEEK_TEAM_INFO,
    'STANDBY_CHANGE_SLOT_1': NavigationAction.STANDBY_CHANGE_SLOT_1,
    'STANDBY_CHANGE_SLOT_2': NavigationAction.STANDBY_CHANGE_SLOT_2,
    'STANDBY_CHANGE_SLOT_3': NavigationAction.STANDBY_CHANGE_SLOT_3,
    'STANDBY_CHANGE_SLOT_4': NavigationAction.STANDBY_CHANGE_SLOT_4,
    'STANDBY_CHANGE_SLOT_5': NavigationAction.STANDBY_CHANGE_SLOT_5,
    'STANDBY_CHANGE_SLOT_6': NavigationAction.STANDBY_CHANGE_SLOT_6,
    'STANDBY_ACTIVATE_DYNAMAX': NavigationAction.STANDBY_ACTIVATE_DYNAMAX,
}


TEST_STATES_JSON = {
    'BATTLE_STATES': [
        {
            'enum_label': 'BATTLE_STATES.STANDBY', # Not used, visual?
            'labels': ['standby'],
            'selectable_overrides': ['menu_option_selected'],
        },
        {
            'enum_label': 'BATTLE_STATES.FIGHT_MENU', # Not used, visual?
            'labels': ['attack_fight_menu'],
            'selectable_overrides': ['attack_option_selected', 'dynamax_selected'],
        },
        {
            'enum_label': 'BATTLE_STATES.ACTIVE_MENU', # Not used, visual?
            'labels': ['active_menu'],
            'selectable_overrides': ['active_menu_pokemon'],
        },
        {
            'enum_label': 'BATTLE_STATES.ACTIVE_POKEMON_STATS', # Not used, visual?
            'labels': ['active_pokemon_info'],
        },
        {
            'enum_label': 'BATTLE_STATES.TEAM_MENU', # Not used, visual?
            'labels': ['team_menu'],
            'selectable_overrides': ['team_pokemon_selected'],
            'cannot_exist_for_override': ['submenu_item_selected'],
        },
        {
            'enum_label': 'BATTLE_STATES.TEAM_MEMBER_OPTIONS_MENU', # Not used, visual?
            'labels': ['team_member_options_menu'],
            'needs_all_for_override': ['team_pokemon_selected', 'submenu_item_selected'],
        },
        {
            'enum_label': 'BATTLE_STATES.POKEMON_SUMMARY_INFO', # Not used, visual?
            'labels': ['pokemon_summary_info'],
        },
        {
            'enum_label': 'BATTLE_STATES.POKEMON_SUMMARY_BASE_STATS', # Not used, visual?
            'labels': ['pokemon_summary_base_stats'],
        },
        {
            'enum_label': 'BATTLE_STATES.POKEMON_SUMMARY_ATTACKS', # Not used, visual?
            'labels': ['pokemon_summary_attacks'],
        },
        {
            'enum_label': 'BATTLE_STATES.MUST_SWITCH', # Not used, visual?
            'labels': ['use_next_pokemon', 'trainer_switch_pokemon'],
        },
        {
            'enum_label': 'BATTLE_STATES.BATTLE_OVER', # Not used, visual?
            'labels': ['wild_battle_over'],
        },
        {
            'enum_label': 'BATTLE_STATES.BATTLE_BEGIN', # Not used, visual?
            'labels': ['wild_pokemon_begin', 'surprise_wild_battle_begin'],
        },
        {
            'enum_label': 'BATTLE_STATES.SWITCH_ERROR_MESSAGE', # Not used, visual?
            'labels': ['switch_error_message'],
        },
        {
            'enum_label': 'BATTLE_STATES.TEAM_MENU_SWITCH_INFO', # Not used, visual?
            'labels': ['team_menu_info_switch'],
        },
        {
            'enum_label': 'BATTLE_STATES.EXPERIENCE_SCREEN', # Not used, visual?
            'labels': ['experience_gained_menu', 'level_up_box'],
        },
        {
            'enum_label': 'BATTLE_STATES.MESSAGE_NEEDS_ACTION', # Not used, visual?
            'labels': ['message_need_action'],
        },
    ]
}



TEST_SELECTABLES_JSON = {
    "BATTLE_SELECTABLES": {
        "enums": [
            {
                "name":"STANDBY_FIGHT",
                "enum_value":0,
                'labels': ['menu_option_selected'],
                "rect":[1039, 409, 1269, 481],
                "network_rect":[1039, 486, 1269, 558],
            },
            {
                "name":"STANDBY_POKEMON",
                "enum_value":1,
                'labels': ['menu_option_selected'],
                "rect":[1039, 486, 1269, 558],
                "network_rect":[1039, 566, 1269, 638],
            },
            {
                "name":"STANDBY_BAG",
                "enum_value":2,
                'labels': ['menu_option_selected'],
                "rect":[1039, 641, 1269, 713],
                "network_rect":[-1, -1, -1, -1],
            },
            {
                "name":"STANDBY_RUN",
                "enum_value":3,
                'labels': ['menu_option_selected'],
                "rect":[1039, 566, 1269, 638],
                "network_rect":[1039, 641, 1269, 713],
            },
            {
                "name":"STANDBY_DYNAMAX",
                "enum_value":4,
                'labels': ['dynamax_selected'],
                "needs_label_only": ["dynamax_selected"],
                "rect":[107, 309, 239, 343],
            },
            {
                "name":"FIGHT_MENU_ATTACK_1",
                "enum_value":5,
                'labels': ['attack_option_selected'],
                "rect":[866, 437, 1272, 498],
                "three_attacks_rect":[866, 437, 1272, 498],
                "two_attacks_rect":[866, 437, 1272, 498],
                "one_attacks_rect":[866, 437, 1272, 498],
            },
            {
                "name":"FIGHT_MENU_ATTACK_2",
                "enum_value":6,
                'labels': ['attack_option_selected'],
                "rect":[866, 502, 1272, 563],
            },
            {
                "name":"FIGHT_MENU_ATTACK_3",
                "enum_value":7,
                'labels': ['attack_option_selected'],
                "rect":[866, 574, 1272, 635],
            },
            {
                "name":"FIGHT_MENU_ATTACK_4",
                "enum_value":8,
                'labels': ['attack_option_selected'],
                "rect":[866, 644, 1272, 705],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_1",
                "enum_value":9,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 63, 449, 159],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_2",
                "enum_value":10,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 162, 449, 258],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_3",
                "enum_value":11,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 255, 449, 351],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_4",
                "enum_value":12,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 351, 449, 447],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_5",
                "enum_value":13,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 451, 449, 547],
            },
            {
                "name":"TEAM_POKEMON_SELECTED_6",
                "enum_value":14,
                'labels': ['team_pokemon_selected'],
                "rect":[57, 551, 449, 647],
            },
        ],
    },
    "BATTLE_SUBMENU_SELECTABLES": {
        "enums": [
            {
                "name":"SWAP_POKEMON",
                "enum_value":0,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [449, 123, 655, 171],
                    '10': [449, 221, 655, 264],
                    '11': [449, 321, 655, 359],
                    '12': [449, 409, 655, 456],
                    '13': [449, 507, 655, 554],
                    '14': [449, 452, 655, 497],
                },
            },
            {
                "name":"CHECK_SUMMARY",
                "enum_value":1,
                'y1_offset': 40,
                'y2_offset': 40,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [449, 123, 655, 171],
                    '10': [449, 221, 655, 264],
                    '11': [449, 321, 655, 359],
                    '12': [449, 409, 655, 456],
                    '13': [449, 507, 655, 554],
                    '14': [449, 452, 655, 497],
                },
            },
            {
                "name":"RESTORE",
                "enum_value":2,
                'y1_offset': 80,
                'y2_offset': 80,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [449, 123, 655, 171],
                    '10': [449, 221, 655, 264],
                    '11': [449, 321, 655, 359],
                    '12': [449, 409, 655, 456],
                    '13': [449, 507, 655, 554],
                    '14': [449, 452, 655, 497],
                },
            },
            {
                "name":"CANCEL",
                "enum_value":3,
                'y1_offset': 120,
                'y2_offset': 120,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [449, 123, 655, 171],
                    '10': [449, 221, 655, 264],
                    '11': [449, 321, 655, 359],
                    '12': [449, 409, 655, 456],
                    '13': [449, 507, 655, 554],
                    '14': [449, 452, 655, 497],
                },
            },
        ],
    },
    "NETWORK_BATTLE_SUBMENU_SELECTABLES": {
        "enums": [
            {
                "name":"SWAP_POKEMON",
                "enum_value":0,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [830, 84, 1037, 129],
                    '10': [830, 181, 1037, 225],
                    '11': [830, 275, 1037, 322],
                    '12': [830, 373, 1037, 415],
                    '13': [830, 466, 1037, 513],
                    '14': [830, 411, 1037, 452],
                },
            },
            {
                "name":"CHECK_SUMMARY",
                "enum_value":1,
                'y1_offset': 40,
                'y2_offset': 40,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [830, 84, 1037, 129],
                    '10': [830, 181, 1037, 225],
                    '11': [830, 275, 1037, 322],
                    '12': [830, 373, 1037, 415],
                    '13': [830, 466, 1037, 513],
                    '14': [830, 411, 1037, 452],
                },
            },
            {
                "name":"RESTORE",
                "enum_value":2,
                'y1_offset': 80,
                'y2_offset': 80,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [830, 84, 1037, 129],
                    '10': [830, 181, 1037, 225],
                    '11': [830, 275, 1037, 322],
                    '12': [830, 373, 1037, 415],
                    '13': [830, 466, 1037, 513],
                    '14': [830, 411, 1037, 452],
                },
            },
            {
                "name":"CANCEL",
                "enum_value":3,
                'y1_offset': 120,
                'y2_offset': 120,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [830, 84, 1037, 129],
                    '10': [830, 181, 1037, 225],
                    '11': [830, 275, 1037, 322],
                    '12': [830, 373, 1037, 415],
                    '13': [830, 466, 1037, 513],
                    '14': [830, 411, 1037, 452],
                },
            },
        ],
    },
}


SEQUENCE_TEMPLATES_JSON = {
    'CHANGE_SLOT_TEMPLATE': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu'],
        'direction': 'vertical',
        'type': 'seek',
        "keys": ["$target_value_1", "$target_value_2", "$submenu_target_value_1"],
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': "$target_value_1",
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_2",
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_2",
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'SWAP_POKEMON', # not used. for visual purposes
                'nested_target_value': "$submenu_target_value_1",
                'on_done': ['Button A'],
            },
        ]
    },
    'ADD_POKEMON_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu', 'pokemon_summary_info', 'pokemon_summary_base_stats', 'pokemon_summary_attacks', ],
        'direction': 'vertical',
        'type': 'seek',
        "keys": ["$target_value_1"],
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'CHECK_SUMMARY', # not used. for visual purposes
                'nested_target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_info',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_base_stats',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_attacks',
                'on_done': ['Button B'],
            },
        ]
    },
    'ATTACK_SLOT': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'attack_fight_menu'],
        'direction': 'vertical',
        'type': 'seek',
        "keys": ["$target_value_1"],
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 0,
                'on_done': ['Button A'],
            },
            {
                'state': 'attack_fight_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': "$target_value_1",
                'on_done': ['Button A'],
            },
        ]
    },
}

TEST_SEQUENCES = {
    'STANDBY_CONSTRUCT_TEAM_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'team_menu',
                'on_done': None,
            },
        ]
    },
    'STANDBY_PEEK_TEAM_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'team_menu',
                'on_done': None,
            },
        ]
    },
    'STANDBY_CHANGE_SLOT_1': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'SWAP_POKEMON', # not used. for visual purposes
                'nested_target_value': 0,
                'on_done': ['Button A'],
            },
        ]
    },
    'STANDBY_CHANGE_SLOT_2': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 10,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_CHANGE_SLOT_3': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 11,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_CHANGE_SLOT_4': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 12,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_CHANGE_SLOT_5': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 13,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_CHANGE_SLOT_6': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 14,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_ADD_FIRST_POKEMON_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu', 'pokemon_summary_info', 'pokemon_summary_base_stats', 'pokemon_summary_attacks', ],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'CHECK_SUMMARY', # not used. for visual purposes
                'nested_target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_info',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_base_stats',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_attacks',
                'on_done': ['Button B'],
            },
        ]
    },
    'STANDBY_ADD_SECOND_POKEMON_INFO': {
        'sequence_template': "ADD_POKEMON_INFO",
        'sequence_template_keys': {
            "$target_value_1": 10,
        },
    },
    'STANDBY_ADD_THIRD_POKEMON_INFO': {
        'sequence_template': "ADD_POKEMON_INFO",
        'sequence_template_keys': {
            "$target_value_1": 11,
        },
    },
    'STANDBY_ADD_FOURTH_POKEMON_INFO': {
        'sequence_template': "ADD_POKEMON_INFO",
        'sequence_template_keys': {
            "$target_value_1": 12,
        },
    },
    'STANDBY_ADD_FIFTH_POKEMON_INFO': {
        'sequence_template': "ADD_POKEMON_INFO",
        'sequence_template_keys': {
            "$target_value_1": 13,
        },
    },
    'STANDBY_ADD_SIXTH_POKEMON_INFO': {
        'sequence_template': "ADD_POKEMON_INFO",
        'sequence_template_keys': {
            "$target_value_1": 14,
        },
    },
    'STANDBY_ATTACK_SLOT_1': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'attack_fight_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 0,
                'on_done': ['Button A'],
            },
            {
                'state': 'attack_fight_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 5,
                'on_done': ['Button A'],
            },
        ]
    },
    'STANDBY_ATTACK_SLOT_2': {
        'sequence_template': "ATTACK_SLOT",
        'sequence_template_keys': {
            "$target_value_1": 6,
        },
    },
    'STANDBY_ATTACK_SLOT_3': {
        'sequence_template': "ATTACK_SLOT",
        'sequence_template_keys': {
            "$target_value_1": 7,
        },
    },
    'STANDBY_ATTACK_SLOT_4': {
        'sequence_template': "ATTACK_SLOT",
        'sequence_template_keys': {
            "$target_value_1": 8,
        },
    },
    'STANDBY_ACTIVATE_DYNAMAX': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'attack_fight_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 0,
                'on_done': ['Button A'],
            },
            {
                'state': 'attack_fight_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_DYNAMAX', # not used. for visual purposes
                'direction': 'horizontal', # trick move left
                'target_value': 4,
                'on_done': ['Button A'],
            },
        ]
    },
    'STANDBY_COUNT_ACTIVE_POKEMON': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'active_menu'],
        'type': 'existence',
        'navigation': [
            {
                'state': 'standby',
                'on_done': ['Button Y'],
            },
            {
                'state': 'active_menu',
                'on_done': None,
            },
        ]
    },
    'STANDBY_CHECK_NEXT_ACTIVE': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['active_menu', 'active_pokemon_info'],
        'type': 'existence',
        'navigation': [
            {
                'state': 'active_menu',
                'on_done': ['Button A'],
            },
            {
                'state': 'active_pokemon_info',
                'on_done': ['Button R'],
            },
        ]
    },
}

crimson_navigator = CrimsonNavigator(TEST_ENUM_MAPPINGS, TEST_SEQUENCES, TEST_STATES_JSON, TEST_SELECTABLES_JSON, SEQUENCE_TEMPLATES_JSON)

def print_game_state(navigation_action, state, selectable, submenu):
    print('nav: %s, bat_state: %s, bat_select: %s, submenu_sel: %s' %(navigation_action, state, selectable, submenu))

def sample_from_frames():
    navigation_action = NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO
    battle_state = None
    battle_selectable = None
    battle_sub_selectable = None
    image_count = 0.0

    images = ['add_info_2_frame_01.png']
    images = []
    for i in range(1, 16):
        image_name = 'add_info_2_frame_%02d.png' % i
        print(image_name)
        image = cv2.imread("poke_test_frames/%s" % (image_name))
        labels_boxes = predict_image(image)

        #change navigation to attack after info added
        if image_name == 'add_info_2_frame_10.png':
            navigation_action = NavigationAction.STANDBY_ATTACK_SLOT_3

        next_battle_state = crimson_navigator.update_state(labels_boxes)
        next_battle_selectable = crimson_navigator.update_selectables(battle_state, labels_boxes)
        next_battle_sub_selectable = crimson_navigator.update_nested_selectables(battle_state, battle_selectable, labels_boxes)

        # Update if battle state isnt None
        if next_battle_state is not None:
            battle_state = next_battle_state
            battle_selectable = next_battle_selectable
            battle_sub_selectable = next_battle_sub_selectable


        print(labels_boxes)
        print('battle_state: %s, battle_selectable: %s, battle_sub_selectable: %s' %(battle_state, battle_selectable, battle_sub_selectable))

        action = crimson_navigator.action_for_state(navigation_action, battle_state, battle_selectable, battle_sub_selectable)
        print(action)
        done = False#check_if_nav_action_finished(navigation_action, battle_state, battle_selectable, battle_sub_selectable, action)
        print_game_state(navigation_action, battle_state, battle_selectable, battle_sub_selectable)
        print('Is nav action done: %s' % (str(done)))

        nav_str = '%s - done?: %s' % (navigation_action, done)
        exp_action_str = 'expected_action: %s' % (action)
        cv2.putText(image, str(battle_state), (200, 200), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(image, str(battle_selectable), (200, 300), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(image, str(battle_sub_selectable), (200, 400), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(image, str(nav_str), (200, 500), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
        cv2.putText(image, str(exp_action_str), (200, 600), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
        save_file = "object_detection_%d.jpg" % (image_count)
        image_count += 1
        cv2.imwrite(save_file, image)


RUNNING = 1
PAUSED = 2
STOPPED = 3
BUTTON_UP = 56
BUTTON_DOWN = 50
BUTTON_LEFT = 52
BUTTON_RIGHT = 54
ESCAPE_KEY = 27
P_KEY = 112
MANUAL_BUTTONS = ['Button A', 'Button B', 'Button X', 'Button Y', 'Button L', 'Button R', 'Button ZL', 'Button ZR', 'LY MAX', 'LY MIN', 'LX MIN', 'LX MAX']
MANUAL_BUTTONS_KEY_PRESS = {97: 'Button A', 98: 'Button B', 120: 'Button X', 121: 'Button Y', 108: 'Button L', 114: 'Button R', BUTTON_DOWN: 'LY MAX', BUTTON_UP: 'LY MIN', BUTTON_LEFT: 'LX MIN', BUTTON_RIGHT: 'LX MAX'}

KEYSTROKE_NAVIGATION = {
    '1': NavigationAction.STANDBY_CONSTRUCT_TEAM_INFO,
    '2': NavigationAction.STANDBY_ADD_FIRST_POKEMON_INFO,
    '3': NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO,
    '4': NavigationAction.STANDBY_ADD_THIRD_POKEMON_INFO,
    '8': NavigationAction.STANDBY_COUNT_ACTIVE_POKEMON,
    '9': NavigationAction.STANDBY_CHECK_NEXT_ACTIVE,
    '5': NavigationAction.STANDBY_ATTACK_SLOT_1,
    '6': NavigationAction.STANDBY_ATTACK_SLOT_2,
    '7': NavigationAction.STANDBY_ATTACK_SLOT_3,
    '0': NavigationAction.STANDBY_CHANGE_SLOT_3,
}

def sample_video_feed():
    battle_state = None
    battle_selectable = None
    battle_sub_selectable = None
    image_count = 0.0

    navigation_action = NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO
    battle_states = BATTLE_STATES.STANDBY
    battle_selectables = BATTLE_SELECTABLES.STANDBY_FIGHT
    submenu_selectables = BATTLE_SUBMENU_SELECTABLES.SWAP_POKEMON
    done = False

    # might not need this
    """
    window_name = "Commands window"
    # CONTROLLER INPUT PROMPT BEGIN
    import numpy as np

    # Create a black image
    img = np.zeros((512,512,3), np.uint8)

    # Write some Text

    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (10,500)
    fontScale              = 1
    fontColor              = (255,255,255)
    lineType               = 2

    cv2.putText(img,'Exists to capture keyboard overrides!',
                bottomLeftCornerOfText,
                font,
                fontScale,
                fontColor,
                lineType)

    #Display the image
    cv2.imshow(window_name,img)
    """


    import json
    runner_config = json.load(open('config/runner_config.json',))

    cap = cv2.VideoCapture(runner_config["device"]) # check this

    """
    currentController = get_360_controller_state()

    currentController = self.preprocessControllerForConsole(currentController)
    self.serial.send(currentController)
    """


    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
#        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.resize(frame, (1280, 720), interpolation = cv2.INTER_AREA)


        labels_boxes = predict_image(frame)


        next_battle_state = crimson_navigator.update_state(labels_boxes)
        next_battle_selectable = crimson_navigator.update_selectables(battle_state, labels_boxes)
        next_battle_sub_selectable = crimson_navigator.update_nested_selectables(battle_state, battle_selectable, labels_boxes)

        # Update if battle state isnt None
        if next_battle_state is not None:
            battle_state = next_battle_state
            battle_selectable = next_battle_selectable
            battle_sub_selectable = next_battle_sub_selectable

#        print(labels_boxes)
        print('battle_state: %s, battle_selectable: %s, battle_sub_selectable: %s' %(battle_state, battle_selectable, battle_sub_selectable))

        action = crimson_navigator.action_for_state(navigation_action, battle_state, battle_selectable, battle_sub_selectable)
        print(action)

        # ESC key pressed
        raw_key = cv2.waitKey(delay=1)
        key = chr(raw_key & 255) if raw_key >= 0 else None
        if key is not None and PRESSED_KEY is None:
            print('key: %s raw_key: %s' % (key, raw_key))
            if key in KEYSTROKE_NAVIGATION:
                navigation_action = KEYSTROKE_NAVIGATION[key]
                print('navigation_action is not', navigation_action)
        #Abandon Session
        if raw_key == ESCAPE_KEY:
            print('Session Abandoned')
            break
        #Force ENd Session
        elif raw_key == P_KEY:
            print('Session Force Ended')
            break


        PRESSED_KEY = key

        nav_str = '%s - done?: %s' % (navigation_action, done)
        exp_action_str = 'expected_action: %s' % (action)
        cv2.putText(frame, str(battle_state), (200, 200), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_selectable), (200, 300), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_sub_selectable), (200, 400), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(nav_str), (200, 500), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
        cv2.putText(frame, str(exp_action_str), (200, 600), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)


        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break



    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()



sample_video_feed()

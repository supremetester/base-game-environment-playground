import enum
import math
import time
import pygame
import xbox360_controller

#class CONTROLLER(enum.Enum):
class CONTROLLER():
    LEFT_STICK_X = 0
    LEFT_STICK_Y = 1
    RIGHT_STICK_X = 2
    RIGHT_STICK_Y = 3
    PAD_UP = 4
    PAD_DOWN = 5
    PAD_LEFT = 6
    PAD_RIGHT = 7
    A = 8
    B = 9
    X = 10
    Y = 11
    LEFT_BUMP = 12
    RIGHT_BUMP = 13
    LEFT_TRIGGER = 14
    RIGHT_TRIGGER = 15

pygame.init()

# window settings
size = [600, 600]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Simple Game")
FPS = 60
clock = pygame.time.Clock()


# make a controller
controller = xbox360_controller.Controller()

#Used for creating button presses and how long they been pressed for
def get_empty_controller_state(timer=False):
    left_trigger = -1
    right_trigger = -1
    if timer:
        left_trigger = 0
        right_trigger = 0

    empty_controller_state = {
        CONTROLLER.LEFT_STICK_X: 0,
        CONTROLLER.LEFT_STICK_Y: 0,
        CONTROLLER.RIGHT_STICK_X: 0,
        CONTROLLER.RIGHT_STICK_Y: 0,
        CONTROLLER.PAD_UP: 0,
        CONTROLLER.PAD_DOWN: 0,
        CONTROLLER.PAD_LEFT: 0,
        CONTROLLER.PAD_RIGHT: 0,
        CONTROLLER.A: 0,
        CONTROLLER.B: 0,
        CONTROLLER.X: 0,
        CONTROLLER.Y: 0,
        CONTROLLER.LEFT_BUMP: 0,
        CONTROLLER.RIGHT_BUMP: 0,
        CONTROLLER.LEFT_TRIGGER: left_trigger,
        CONTROLLER.RIGHT_TRIGGER: right_trigger,
    }

    return empty_controller_state

def get_controller_state():
    raw_button_state = controller.get_buttons()
    triggers = controller.get_triggers()
    up, right, down, left = controller.get_pad()


    (left_stick_x, left_stick_y) = controller.get_left_stick()
    (right_stick_x, right_stick_y) = controller.get_right_stick()

    #UP,DOWN,L,R
    PAD_UP = up
    PAD_DOWN = down
    PAD_LEFT = left
    PAD_RIGHT = right
    #UP,DOWN,L,R
    A = raw_button_state[11]
    B = raw_button_state[12]
    X = raw_button_state[13]
    Y = raw_button_state[14]
    #LB,RB,LT,RTR
    LEFT_BUMP = raw_button_state[8]
    RIGHT_BUMP = raw_button_state[9]
    LEFT_TRIGGER = triggers
    RIGHT_TRIGGER = triggers
    left_stick_x = round(left_stick_x * 128)
    left_stick_y = round(left_stick_y * 128)
    right_stick_x = round(right_stick_x * 128)
    right_stick_y = round(right_stick_y * 128)

    controller_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: PAD_UP,
        CONTROLLER.PAD_DOWN: PAD_DOWN,
        CONTROLLER.PAD_LEFT: PAD_LEFT,
        CONTROLLER.PAD_RIGHT: PAD_RIGHT,
        CONTROLLER.A: A,
        CONTROLLER.B: B,
        CONTROLLER.X: X,
        CONTROLLER.Y: Y,
        CONTROLLER.LEFT_BUMP: LEFT_BUMP,
        CONTROLLER.RIGHT_BUMP: RIGHT_BUMP,
        CONTROLLER.LEFT_TRIGGER: round(LEFT_TRIGGER),
        CONTROLLER.RIGHT_TRIGGER:round(RIGHT_TRIGGER),
    }

    controller_state2 = [left_stick_x, left_stick_y, right_stick_x, right_stick_y, PAD_DOWN, PAD_DOWN, PAD_LEFT, PAD_RIGHT, A, B, X, Y, LEFT_BUMP, RIGHT_BUMP, round(LEFT_TRIGGER), round(RIGHT_TRIGGER)]

    return controller_state, ','.join(str(x) for x in controller_state2)


def get_controller_time(controller_state, old_controller_timing_state, current_time):
    (left_stick_x, left_stick_y) = (0, 0)
    (right_stick_x, right_stick_y) = (0, 0)

    pad_up = 0
    pad_down = 0
    pad_left = 0
    pad_right = 0
    A = 0
    B = 0
    X = 0
    Y = 0
    left_bump = 0
    right_bump = 0
    left_trigger = -1
    right_trigger = -1

    if controller_state[CONTROLLER.LEFT_STICK_X] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_STICK_X] == 0:
            left_stick_x = current_time
        else:
            left_stick_x = old_controller_timing_state[CONTROLLER.LEFT_STICK_X]
    if controller_state[CONTROLLER.LEFT_STICK_Y] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_STICK_Y] == 0:
            left_stick_y = current_time
        else:
            left_stick_y = old_controller_timing_state[CONTROLLER.LEFT_STICK_Y]
    if controller_state[CONTROLLER.RIGHT_STICK_X] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_STICK_X] == 0:
            right_stick_x = current_time
        else:
            right_stick_x = old_controller_timing_state[CONTROLLER.RIGHT_STICK_X]
    if controller_state[CONTROLLER.RIGHT_STICK_Y] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_STICK_Y] == 0:
            right_stick_y = current_time
        else:
            right_stick_y = old_controller_timing_state[CONTROLLER.RIGHT_STICK_Y]
    if controller_state[CONTROLLER.PAD_UP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_UP] == 0:
            pad_up = current_time
        else:
            pad_up = old_controller_timing_state[CONTROLLER.PAD_UP]
    if controller_state[CONTROLLER.PAD_DOWN] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_DOWN] == 0:
            pad_down = current_time
        else:
            pad_down = old_controller_timing_state[CONTROLLER.PAD_DOWN]
    if controller_state[CONTROLLER.PAD_LEFT] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_LEFT] == 0:
            pad_left = current_time
        else:
            pad_left = old_controller_timing_state[CONTROLLER.PAD_LEFT]
    if controller_state[CONTROLLER.PAD_RIGHT] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_RIGHT] == 0:
            pad_right = current_time
        else:
            pad_right = old_controller_timing_state[CONTROLLER.PAD_RIGHT]
    if controller_state[CONTROLLER.A] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.A] == 0:
            A = current_time
        else:
            A = old_controller_timing_state[CONTROLLER.A]
    if controller_state[CONTROLLER.B] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.B] == 0:
            B = current_time
        else:
            B = old_controller_timing_state[CONTROLLER.B]
    if controller_state[CONTROLLER.X] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.X] == 0:
            X = current_time
        else:
            X = old_controller_timing_state[CONTROLLER.X]
    if controller_state[CONTROLLER.Y] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.Y] == 0:
            Y = current_time
        else:
            Y = old_controller_timing_state[CONTROLLER.Y]
    if controller_state[CONTROLLER.LEFT_BUMP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_BUMP] == 0:
            left_bump = current_time
        else:
            left_bump = old_controller_timing_state[CONTROLLER.LEFT_BUMP]
    if controller_state[CONTROLLER.RIGHT_BUMP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_BUMP] == 0:
            right_bump = current_time
        else:
            right_bump = old_controller_timing_state[CONTROLLER.RIGHT_BUMP]
    if controller_state[CONTROLLER.LEFT_TRIGGER] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_TRIGGER] == 0:
            left_trigger = current_time
        else:
            left_trigger = old_controller_timing_state[CONTROLLER.LEFT_TRIGGER]
    if controller_state[CONTROLLER.RIGHT_TRIGGER] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_TRIGGER] == 0:
            right_trigger = current_time
        else:
            right_trigger = old_controller_timing_state[CONTROLLER.RIGHT_TRIGGER]

    controller_timing_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: pad_up,
        CONTROLLER.PAD_DOWN: pad_down,
        CONTROLLER.PAD_LEFT: pad_left,
        CONTROLLER.PAD_RIGHT: pad_right,
        CONTROLLER.A: A,
        CONTROLLER.B: B,
        CONTROLLER.X: X,
        CONTROLLER.Y: Y,
        CONTROLLER.LEFT_BUMP: left_bump,
        CONTROLLER.RIGHT_BUMP: right_bump,
        CONTROLLER.LEFT_TRIGGER: left_trigger,
        CONTROLLER.RIGHT_TRIGGER: right_trigger,
    }

    return controller_timing_state

def calculate_field_difference(prev_controller, curr_controller):
    differences = []
    for key in prev_controller:
        pre_val = prev_controller[key]
        curr_val = curr_controller[key]
        if pre_val != curr_val:
            differences.append((key, curr_controller[key]))
    return differences



prev_controller, curr_controller = get_empty_controller_state(), get_empty_controller_state()
prev_timer, curr_timer = get_empty_controller_state(True), get_empty_controller_state(True)

while True:
    # event handling
    pygame.event.get()
    current_time = time.time()

    curr_controller, statooo = get_controller_state()
    curr_timer = get_controller_time(curr_controller, prev_timer, current_time)
    buttons_to_update = calculate_field_difference(prev_controller, curr_controller)
    if len(buttons_to_update) > 0:
        for key, value in buttons_to_update:
            print('%s: %s time:%s' % (key, value, curr_timer[key]))
    print(buttons_to_update)
    print(statooo)

    prev_controller = curr_controller
    prev_timer = curr_timer

    # update screen
    clock.tick(FPS)

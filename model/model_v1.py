# @Author: Dwivedi Chandan
# @Date:   2019-08-05T13:35:05+05:30
# @Email:  chandandwivedi795@gmail.com
# @Last modified by:   Dwivedi Chandan
# @Last modified time: 2019-08-07T11:52:45+05:30


# import the necessary packages
import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Input, Concatenate
import numpy as np
import sys
if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

import pandas as pd
import cv2

import argparse
import time
import os
import jsonpickle
#import binascii
import io
import json
from PIL import Image
import base64
import os

import uuid

try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3



import os
DEBUG = bool(int(os.environ.get('DEBUG', 0)))

import json
load_config = json.load(open('config/model_config.json',))

try:
    loaded_model_v1 = tf.keras.models.load_model(load_config['model_location'])
    mapping = pd.read_csv(load_config['mapping_location'])
    print(mapping.head())
except:
    print("Error loading ml inference model")

# route http posts to this method
def get_model_v1_prediction(image, controller_frame):
    use_controller = not load_config["only_images"]
    print('controller_frame', controller_frame)

    frame_data = StringIO(controller_frame)
    csv_features = pd.read_csv(frame_data, header=None)

    #resize image for network
    test_img = res = cv2.resize(image, dsize=(128, 128), interpolation=cv2.INTER_CUBIC)
    test_img = test_img.reshape((1,128, 128,3))

    if use_controller:
        y_prob = loaded_model_v1.predict([test_img, csv_features])
    else:
        y_prob = loaded_model_v1.predict([test_img])
    img_class = y_prob.argmax(axis=-1)
    prediction = img_class[0]
    print('prediction', prediction)
    controller_pred = mapping.loc[mapping['id'] == prediction, :]['label'].item()

    return controller_pred

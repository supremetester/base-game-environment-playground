from navigator.crimson_navigator import *
from navigator.game_navigator_state_common import *
import cv2
from yolo_v3.yolov3_manager import *


TEST_ENUM_MAPPINGS = {
    'GAME_STATES': GAME_STATES,
    'GAME_SELECTABLES': GAME_SELECTABLES,
    'GAME_SUB_SELECTABLES': GAME_SUB_SELECTABLES,


    'SELECT_MEGAMAX_X': NavigationAction.SELECT_MEGAMAX_X,
    'SELECT_MEGAMAX_X2': NavigationAction.SELECT_MEGAMAX_X2,
    'SELECT_CHALLENGE': NavigationAction.SELECT_CHALLENGE,
    'SELECT_START_GAME': NavigationAction.SELECT_START_GAME,
    'SELECT_PASSWORD': NavigationAction.SELECT_PASSWORD,
    'SELECT_OPTIONS_MODE': NavigationAction.SELECT_OPTIONS_MODE,
}


TEST_STATES_JSON = {
    'GAME_STATES': [
        {
            'enum_label': 'GAME_STATES.GAME_PICKER', # Not used, visual?
            'labels': ['game_picker'],
            'selectable_overrides': ['game_picker_menu_item_selected'],
        },
        {
            'enum_label': 'GAME_STATES.GAME_PICKER_MENU_OPTIONS', # Not used, visual?
            'labels': ['game_picker_submenu_options'],
            'selectable_overrides': ['game_picker_submenu_item_selected'],
        },
        {
            'enum_label': 'GAME_STATES.X2_TITLE_MENU', # Not used, visual?
            'labels': ['title_screen'],
            'selectable_overrides': ['title_screen_menu_option_selected'],
        },
    ]
}

TEST_SELECTABLES_JSON = {
    "GAME_SELECTABLES": {
        "enums": [
            {
                "name":"MEGAMAX_X",
                "enum_value":0,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[1001, 51, 1223, 129],
            },
            {
                "name":"MEGAMAX_X2",
                "enum_value":1,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[995, 131, 1214, 203],
            },
            {
                "name":"MEGAMAX_X3",
                "enum_value":2,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[996, 208, 1216, 283],
            },
            {
                "name":"MEGAMAX_X4",
                "enum_value":3,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[1002, 284, 1215, 354],
            },
            {
                "name":"OPTIONS",
                "enum_value":4,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[998, 354, 1222, 427],
            },
            {
                "name":"MUSEUM",
                "enum_value":5,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[1000, 431, 1218, 500],
            },
            {
                "name":"CHALLENGE",
                "enum_value":6,
                'labels': ['game_picker_menu_item_selected'],
                "rect":[985, 503, 1220, 579],
            },

            {
                "name":"START_GAME",
                "enum_value":7,
                'labels': ['title_screen_menu_option_selected'],
                "rect":[442, 487, 793, 545],
            },
            {
                "name":"PASSWORD",
                "enum_value":8,
                'labels': ['title_screen_menu_option_selected'],
                "rect":[441, 547, 787, 596],
            },
            {
                "name":"OPTIONS_MODE",
                "enum_value":9,
                'labels': ['title_screen_menu_option_selected'],
                "rect":[444, 599, 786, 656],
            },

        ],
    },
    "GAME_SUB_SELECTABLES": {
        "enums": [
            {
                "name":"START",
                "enum_value":0,
                'labels': ['game_picker_submenu_item_selected'],
                "rects":{
                    '0': [617, 118, 883, 171],
                    '1': [617, 118, 883, 171],
                    '2': [617, 118, 883, 171],
                    '3': [617, 118, 883, 171],
                    '4': [617, 118, 883, 171],
                    '5': [617, 118, 883, 171],
                    '6': [617, 118, 883, 171],
                },
            },
            {
                "name":"ROOKIE_MODE",
                "enum_value":1,
                'labels': ['game_picker_submenu_item_selected'],
                "rects":{
                    '0': [633, 171, 881, 243],
                    '1': [633, 171, 881, 243],
                    '2': [633, 171, 881, 243],
                    '3': [633, 171, 881, 243],
                    '4': [633, 171, 881, 243],
                    '5': [633, 171, 881, 243],
                    '6': [633, 171, 881, 243],
                },
            },
        ],
    },
}

SEQUENCE_TEMPLATES_JSON = {
    'ADD_POKEMON_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu', 'pokemon_summary_info', 'pokemon_summary_base_stats', 'pokemon_summary_attacks', ],
        'direction': 'vertical',
        'type': 'seek',
        "keys": ["$target_value_1"],
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'CHECK_SUMMARY', # not used. for visual purposes
                'nested_target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_info',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_base_stats',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_attacks',
                'on_done': ['Button B'],
            },
        ]
    },
}

TEST_SEQUENCES = {

    'SELECT_MEGAMAX_X': {
        'state_enum': 'GAME_STATES',
        'supported_states': ['game_picker', 'game_picker_submenu_options'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'game_picker',
                'selectable': 'GAME_SELECTABLES',
                'selectable_name': 'MEGAMAX_X',
                'target_value': 0,
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'game_picker_submenu_options',
                'selectable': 'GAME_SELECTABLES',
                'selectable_name': 'MEGAMAX_X', # not used. for visual purposes
                'target_value': 0,
                'nested_selectable': 'GAME_SUB_SELECTABLES',
                'nested_selectable_name': 'START', # not used. for visual purposes
                'nested_target_value': 0,
                'on_done': ['Button A'],
            }
        ]
    },
}

crimson_navigator = CrimsonNavigator(TEST_ENUM_MAPPINGS, TEST_SEQUENCES, TEST_STATES_JSON, TEST_SELECTABLES_JSON, SEQUENCE_TEMPLATES_JSON)

def print_game_state(navigation_action, state, selectable, submenu):
    print('nav: %s, bat_state: %s, bat_select: %s, submenu_sel: %s' %(navigation_action, state, selectable, submenu))

RUNNING = 1
PAUSED = 2
STOPPED = 3
BUTTON_UP = 56
BUTTON_DOWN = 50
BUTTON_LEFT = 52
BUTTON_RIGHT = 54
ESCAPE_KEY = 27
P_KEY = 112
MANUAL_BUTTONS = ['Button A', 'Button B', 'Button X', 'Button Y', 'Button L', 'Button R', 'Button ZL', 'Button ZR', 'LY MAX', 'LY MIN', 'LX MIN', 'LX MAX']
MANUAL_BUTTONS_KEY_PRESS = {97: 'Button A', 98: 'Button B', 120: 'Button X', 121: 'Button Y', 108: 'Button L', 114: 'Button R', BUTTON_DOWN: 'LY MAX', BUTTON_UP: 'LY MIN', BUTTON_LEFT: 'LX MIN', BUTTON_RIGHT: 'LX MAX'}


################# Fill in with you own Navigations
KEYSTROKE_NAVIGATION = {
    '1': NavigationAction.SELECT_MEGAMAX_X,
    '2': NavigationAction.SELECT_MEGAMAX_X,
    '3': NavigationAction.SELECT_MEGAMAX_X,
    '4': NavigationAction.SELECT_MEGAMAX_X,
    '8': NavigationAction.SELECT_MEGAMAX_X,
    '9': NavigationAction.SELECT_MEGAMAX_X,
    '5': NavigationAction.SELECT_MEGAMAX_X,
    '6': NavigationAction.SELECT_MEGAMAX_X,
    '7': NavigationAction.SELECT_MEGAMAX_X,
    '0': NavigationAction.SELECT_MEGAMAX_X,
}

def sample_video_feed():
    battle_state = None
    battle_selectable = None
    battle_sub_selectable = None
    image_count = 0.0

    ############### Assign starting values
    navigation_action = NavigationAction.SELECT_MEGAMAX_X
    battle_states = None
    battle_selectables = None
    submenu_selectables = None


    done = False

    import json
    runner_config = json.load(open('config/runner_config.json',))

    cap = cv2.VideoCapture(runner_config["device"]) # check this

    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
#        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.resize(frame, (1280, 720), interpolation = cv2.INTER_AREA)


        labels_boxes = predict_image(frame)


        next_battle_state = crimson_navigator.update_state(labels_boxes)
        next_battle_selectable = crimson_navigator.update_selectables(battle_state, labels_boxes)
        next_battle_sub_selectable = crimson_navigator.update_nested_selectables(battle_state, battle_selectable, labels_boxes)

        # Update if battle state isnt None
        if next_battle_state is not None:
            battle_state = next_battle_state
            battle_selectable = next_battle_selectable
            battle_sub_selectable = next_battle_sub_selectable

#        print(labels_boxes)
        print('battle_state: %s, battle_selectable: %s, battle_sub_selectable: %s' %(battle_state, battle_selectable, battle_sub_selectable))

        action = crimson_navigator.action_for_state(navigation_action, battle_state, battle_selectable, battle_sub_selectable)
        print(action)

        # ESC key pressed
        raw_key = cv2.waitKey(delay=1)
        key = chr(raw_key & 255) if raw_key >= 0 else None
        if key is not None and PRESSED_KEY is None:
            print('key: %s raw_key: %s' % (key, raw_key))
            if key in KEYSTROKE_NAVIGATION:
                navigation_action = KEYSTROKE_NAVIGATION[key]
                print('navigation_action is not', navigation_action)
        #Abandon Session
        if raw_key == ESCAPE_KEY:
            print('Session Abandoned')
            break
        #Force ENd Session
        elif raw_key == P_KEY:
            print('Session Force Ended')
            break


        PRESSED_KEY = key

        nav_str = '%s - done?: %s' % (navigation_action, done)
        exp_action_str = 'expected_action: %s' % (action)
        cv2.putText(frame, str(battle_state), (200, 200), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_selectable), (200, 300), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_sub_selectable), (200, 400), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(nav_str), (200, 500), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
        cv2.putText(frame, str(exp_action_str), (200, 600), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)


        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break



    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()



sample_video_feed()

import enum
import math
import time
import pygame
import controller.xbox360_controller
import math
import json

#class CONTROLLER(enum.Enum):
class CONTROLLER():
    LEFT_STICK_X = 0
    LEFT_STICK_Y = 1
    RIGHT_STICK_X = 2
    RIGHT_STICK_Y = 3
    PAD_UP = 4
    PAD_DOWN = 5
    PAD_LEFT = 6
    PAD_RIGHT = 7
    A = 8
    B = 9
    X = 10
    Y = 11
    LEFT_BUMP = 12
    RIGHT_BUMP = 13
    LEFT_TRIGGER = 14
    RIGHT_TRIGGER = 15

pygame.init()

# window settings
#size = [600, 600]
#screen = pygame.display.set_mode(size)
#pygame.display.set_caption("Simple Game")
FPS = 60
clock = pygame.time.Clock()

runner_config = json.load(open('config/runner_config.json',))

# make a controller
controller = controller.xbox360_controller.Controller()


def get_360_controller_state():
    pygame.event.get()
    raw_button_state = controller.get_buttons()
    triggers = controller.get_triggers()
    up, right, down, left = controller.get_pad()


    (left_stick_x, left_stick_y) = controller.get_left_stick()
    (right_stick_x, right_stick_y) = controller.get_right_stick()

    #UP,DOWN,L,R
    PAD_UP = up
    PAD_DOWN = down
    PAD_LEFT = left
    PAD_RIGHT = right
    #A,B,X,Y
    A = raw_button_state[11]
    B = raw_button_state[12]
    X = raw_button_state[13]
    Y = raw_button_state[14]
    #LB,RB,LT,RTR
    LEFT_BUMP = raw_button_state[8]
    RIGHT_BUMP = raw_button_state[9]
    LEFT_TRIGGER = triggers
    RIGHT_TRIGGER = triggers

    base = runner_config['round_controler_sticks_to_nearest']
    #If controller isnt neutral i.e. 128, round to nearest 5
    #should drastically shrink categories

#    left_stick_x = base * round(left_stick_x * 128 / base) + 128
#    left_stick_y = base * round(left_stick_y * 128 / base) + 128
#    right_stick_x = base * round(right_stick_x * 128 / base) + 128
#    right_stick_y = base * round(right_stick_y * 128 / base) + 128
    left_stick_x = round(left_stick_x * 127) + 128
    left_stick_y = round(left_stick_y * 127) + 128
    right_stick_x = round(right_stick_x * 127) + 128
    right_stick_y = round(right_stick_y * 127) + 128

    controller_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: PAD_UP,
        CONTROLLER.PAD_DOWN: PAD_DOWN,
        CONTROLLER.PAD_LEFT: PAD_LEFT,
        CONTROLLER.PAD_RIGHT: PAD_RIGHT,
        CONTROLLER.A: A,
        CONTROLLER.B: B,
        CONTROLLER.X: X,
        CONTROLLER.Y: Y,
        CONTROLLER.LEFT_BUMP: LEFT_BUMP,
        CONTROLLER.RIGHT_BUMP: RIGHT_BUMP,
        CONTROLLER.LEFT_TRIGGER: round(LEFT_TRIGGER),
        CONTROLLER.RIGHT_TRIGGER:round(RIGHT_TRIGGER),
    }

    controller_state2 = [left_stick_x, left_stick_y, right_stick_x, right_stick_y, PAD_UP, PAD_DOWN, PAD_LEFT, PAD_RIGHT, A, B, X, Y, LEFT_BUMP, RIGHT_BUMP, round(LEFT_TRIGGER), round(RIGHT_TRIGGER)]

    return ','.join(str(x) for x in controller_state2)

clock.tick(FPS)

import numpy as np
import cv2
import os
import uuid
import threading

def open_cam_usb(dev):
    # We want to set width and height here, otherwise we could just do:
    #     return cv2.VideoCapture(dev)
    gst_str = ("v4l2src device=/dev/video{} ! "
	       "image/jpeg, format=(string)RGGB ! "
	       " jpegdec ! videoconvert ! queue ! appsink").format(dev)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)


import json
runner_config = json.load(open('config/runner_config.json',))

cap = cv2.VideoCapture(runner_config["device"]) # check this

#cap = open_cam_usb("2")

gst_str = ("v4l2src device=/dev/video2 ! "
       "image/jpeg, format=(string)RGGB ! "
       " jpegdec ! videoconvert ! queue ! appsink")
#cap=cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
GAME_NAME = os.environ.get('GAME_NAME', "data_upload")
SHOW_PREVIEW = bool(int(os.environ.get('SHOW_PREVIEW', 1)))
THREADED = bool(int(os.environ.get('THREADED', 0)))


filename_base = str(uuid.uuid4())

if not os.path.exists(os.path.dirname(GAME_NAME+'/')):
    os.makedirs(os.path.dirname(GAME_NAME+'/'))

def save_image(filename, image):
    cv2.imwrite(filename, image)

import time
time.sleep(0.1)
frame_count = 0

frame_rate = runner_config['screenshot_grabber_max_fps']
prev = 0

while(True):

    # Capture frame-by-frame
    ret, frame = cap.read()

    time_elapsed = time.time() - prev

    if time_elapsed > 1./frame_rate:
        prev = time.time()
    else:
        continue


    height , width , layers =  frame.shape
    new_h=720
    new_w=1280
    imageSrc = cv2.resize(frame, (new_w, new_h))


    filename = "%s/%s-%s.jpg" % (GAME_NAME, filename_base, str(frame_count).zfill(5))
    if THREADED:
        t = threading.Thread(target=save_image, args=[filename, imageSrc])
        t.start()
    else:
        save_image(filename, imageSrc)

    frame_count += 1
    print('frames collected.', frame_count)

    if runner_config['screenshot_grabber_show_preview']:
        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        cv2.imshow('frame',frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

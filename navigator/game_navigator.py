from crimson_navigator import *
from poke_state_common import *
import cv2
from yolo_manager import *


TEST_ENUM_MAPPINGS = {
    'BATTLE_STATES': BATTLE_STATES,
    'BATTLE_SELECTABLES': BATTLE_SELECTABLES,
    'BATTLE_SUBMENU_SELECTABLES': BATTLE_SUBMENU_SELECTABLES,
    'NETWORK_BATTLE_SUBMENU_SELECTABLES': NETWORK_BATTLE_SUBMENU_SELECTABLES,


    'STANDBY_CONSTRUCT_TEAM_INFO': NavigationAction.STANDBY_CONSTRUCT_TEAM_INFO,
    'STANDBY_ADD_FIRST_POKEMON_INFO': NavigationAction.STANDBY_ADD_FIRST_POKEMON_INFO,
    'STANDBY_ADD_SECOND_POKEMON_INFO': NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO,
    'STANDBY_ATTACK_SLOT_2': NavigationAction.STANDBY_ATTACK_SLOT_2,
    'STANDBY_ATTACK_SLOT_3': NavigationAction.STANDBY_ATTACK_SLOT_3,
    'STANDBY_ATTACK_SLOT_4': NavigationAction.STANDBY_ATTACK_SLOT_4,
    'STANDBY_PEEK_TEAM_INFO': NavigationAction.STANDBY_PEEK_TEAM_INFO,
    'STANDBY_CHANGE_SLOT_1': NavigationAction.STANDBY_CHANGE_SLOT_1,
    'STANDBY_ACTIVATE_DYNAMAX': NavigationAction.STANDBY_ACTIVATE_DYNAMAX,
}


TEST_STATES_JSON = {
    'BATTLE_STATES': [
        {
            'enum_label': 'BATTLE_STATES.STANDBY', # Not used, visual?
            'labels': ['standby'],
            'selectable_overrides': ['menu_option_selected'],
        },
        {
            'enum_label': 'BATTLE_STATES.FIGHT_MENU', # Not used, visual?
            'labels': ['attack_fight_menu'],
            'selectable_overrides': ['attack_option_selected', 'dynamax_selected'],
        },
    ]
}

TEST_SELECTABLES_JSON = {
    "BATTLE_SELECTABLES": {
        "enums": [
            {
                "name":"STANDBY_DYNAMAX",
                "enum_value":4,
                'labels': ['dynamax_selected'],
                "needs_label_only": ["dynamax_selected"],
                "rect":[107, 309, 239, 343],
            },
            {
                "name":"FIGHT_MENU_ATTACK_1",
                "enum_value":5,
                'labels': ['attack_option_selected'],
                "rect":[866, 437, 1272, 498],
                "three_attacks_rect":[866, 437, 1272, 498],
                "two_attacks_rect":[866, 437, 1272, 498],
                "one_attacks_rect":[866, 437, 1272, 498],
            },
            {
                "name":"FIGHT_MENU_ATTACK_2",
                "enum_value":6,
                'labels': ['attack_option_selected'],
                "rect":[866, 502, 1272, 563],
            },
        ],
    },
    "BATTLE_SUBMENU_SELECTABLES": {
        "enums": [
            {
                "name":"SWAP_POKEMON",
                "enum_value":0,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [449, 123, 655, 171],
                    '10': [449, 221, 655, 264],
                    '11': [449, 321, 655, 359],
                    '12': [449, 409, 655, 456],
                    '13': [449, 507, 655, 554],
                    '14': [449, 452, 655, 497],
                },
            },
        ],
    },
    "NETWORK_BATTLE_SUBMENU_SELECTABLES": {
        "enums": [
            {
                "name":"SWAP_POKEMON",
                "enum_value":0,
                'labels': ['submenu_item_selected'],
                "rects":{
                    '9': [830, 84, 1037, 129],
                    '10': [830, 181, 1037, 225],
                    '11': [830, 275, 1037, 322],
                    '12': [830, 373, 1037, 415],
                    '13': [830, 466, 1037, 513],
                    '14': [830, 411, 1037, 452],
                },
            },
        ],
    },
}

SEQUENCE_TEMPLATES_JSON = {
    'ADD_POKEMON_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu', 'pokemon_summary_info', 'pokemon_summary_base_stats', 'pokemon_summary_attacks', ],
        'direction': 'vertical',
        'type': 'seek',
        "keys": ["$target_value_1"],
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': "$target_value_1",
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'CHECK_SUMMARY', # not used. for visual purposes
                'nested_target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_info',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_base_stats',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_attacks',
                'on_done': ['Button B'],
            },
        ]
    },
}

TEST_SEQUENCES = {
    'STANDBY_CHANGE_SLOT_6': {
        'sequence_template': "CHANGE_SLOT_TEMPLATE",
        'sequence_template_keys': {
            "$target_value_1": 1,
            "$target_value_2": 14,
            "$submenu_target_value_1": 0,
        },
    },
    'STANDBY_ADD_FIRST_POKEMON_INFO': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'team_menu', 'team_member_options_menu', 'pokemon_summary_info', 'pokemon_summary_base_stats', 'pokemon_summary_attacks', ],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'state': 'team_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'on_done': ['Button A'],
            },
            {
                'direction': 'vertical',
                'state': 'team_member_options_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'TEAM_POKEMON_SELECTED_1', # not used. for visual purposes
                'target_value': 9,
                'nested_selectable': 'BATTLE_SUBMENU_SELECTABLES',
                'nested_selectable_name': 'CHECK_SUMMARY', # not used. for visual purposes
                'nested_target_value': 1,
                'on_done': ['Button A'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_info',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_base_stats',
                'on_done': ['LX MAX'],
            },
            {
                'type': 'existence',
                'state': 'pokemon_summary_attacks',
                'on_done': ['Button B'],
            },
        ]
    },
    'STANDBY_ATTACK_SLOT_3': {
        'sequence_template': "ATTACK_SLOT",
        'sequence_template_keys': {
            "$target_value_1": 7,
        },
    },
    'STANDBY_ATTACK_SLOT_4': {
        'sequence_template': "ATTACK_SLOT",
        'sequence_template_keys': {
            "$target_value_1": 8,
        },
    },
    'STANDBY_ACTIVATE_DYNAMAX': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'attack_fight_menu'],
        'direction': 'vertical',
        'type': 'seek',
        'navigation': [
            {
                'state': 'standby',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_POKEMON', # not used. for visual purposes
                'target_value': 0,
                'on_done': ['Button A'],
            },
            {
                'state': 'attack_fight_menu',
                'selectable': 'BATTLE_SELECTABLES',
                'selectable_name': 'STANDBY_DYNAMAX', # not used. for visual purposes
                'direction': 'horizontal', # trick move left
                'target_value': 4,
                'on_done': ['Button A'],
            },
        ]
    },
    'STANDBY_COUNT_ACTIVE_POKEMON': {
        'state_enum': 'BATTLE_STATES',
        'supported_states': ['standby', 'active_menu'],
        'type': 'existence',
        'navigation': [
            {
                'state': 'standby',
                'on_done': ['Button Y'],
            },
            {
                'state': 'active_menu',
                'on_done': None,
            },
        ]
    },
}

crimson_navigator = CrimsonNavigator(TEST_ENUM_MAPPINGS, TEST_SEQUENCES, TEST_STATES_JSON, TEST_SELECTABLES_JSON, SEQUENCE_TEMPLATES_JSON)

def print_game_state(navigation_action, state, selectable, submenu):
    print('nav: %s, bat_state: %s, bat_select: %s, submenu_sel: %s' %(navigation_action, state, selectable, submenu))

RUNNING = 1
PAUSED = 2
STOPPED = 3
BUTTON_UP = 56
BUTTON_DOWN = 50
BUTTON_LEFT = 52
BUTTON_RIGHT = 54
ESCAPE_KEY = 27
P_KEY = 112
MANUAL_BUTTONS = ['Button A', 'Button B', 'Button X', 'Button Y', 'Button L', 'Button R', 'Button ZL', 'Button ZR', 'LY MAX', 'LY MIN', 'LX MIN', 'LX MAX']
MANUAL_BUTTONS_KEY_PRESS = {97: 'Button A', 98: 'Button B', 120: 'Button X', 121: 'Button Y', 108: 'Button L', 114: 'Button R', BUTTON_DOWN: 'LY MAX', BUTTON_UP: 'LY MIN', BUTTON_LEFT: 'LX MIN', BUTTON_RIGHT: 'LX MAX'}


################# Fill in with you own Navigations
KEYSTROKE_NAVIGATION = {
    '1': NavigationAction.STANDBY_CONSTRUCT_TEAM_INFO,
    '2': None,
    '3': None,
    '4': None,
    '8': None,
    '9': None,
    '5': None,
    '6': None,
    '7': None,
    '0': None,
}

def sample_video_feed():
    battle_state = None
    battle_selectable = None
    battle_sub_selectable = None
    image_count = 0.0

    ############### Assign starting values
    navigation_action = NavigationAction.STANDBY_ADD_SECOND_POKEMON_INFO
    battle_states = None
    battle_selectables = None
    submenu_selectables = None


    done = False

    cap = cv2.VideoCapture(4)
    yolo_manager = YoloManager()

    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
#        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.resize(frame, (1280, 720), interpolation = cv2.INTER_AREA)


        labels_boxes = yolo_manager.predict_image(frame)


        next_battle_state = crimson_navigator.update_state(labels_boxes)
        next_battle_selectable = crimson_navigator.update_selectables(battle_state, labels_boxes)
        next_battle_sub_selectable = crimson_navigator.update_nested_selectables(battle_state, battle_selectable, labels_boxes)

        # Update if battle state isnt None
        if next_battle_state is not None:
            battle_state = next_battle_state
            battle_selectable = next_battle_selectable
            battle_sub_selectable = next_battle_sub_selectable

#        print(labels_boxes)
        print('battle_state: %s, battle_selectable: %s, battle_sub_selectable: %s' %(battle_state, battle_selectable, battle_sub_selectable))

        action = crimson_navigator.action_for_state(navigation_action, battle_state, battle_selectable, battle_sub_selectable)
        print(action)

        # ESC key pressed
        raw_key = cv2.waitKey(delay=1)
        key = chr(raw_key & 255) if raw_key >= 0 else None
        if key is not None and PRESSED_KEY is None:
            print('key: %s raw_key: %s' % (key, raw_key))
            if key in KEYSTROKE_NAVIGATION:
                navigation_action = KEYSTROKE_NAVIGATION[key]
                print('navigation_action is not', navigation_action)
        #Abandon Session
        if raw_key == ESCAPE_KEY:
            print('Session Abandoned')
            break
        #Force ENd Session
        elif raw_key == P_KEY:
            print('Session Force Ended')
            break


        PRESSED_KEY = key

        nav_str = '%s - done?: %s' % (navigation_action, done)
        exp_action_str = 'expected_action: %s' % (action)
        cv2.putText(frame, str(battle_state), (200, 200), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_selectable), (200, 300), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(battle_sub_selectable), (200, 400), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 0), 2)
        cv2.putText(frame, str(nav_str), (200, 500), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
        cv2.putText(frame, str(exp_action_str), (200, 600), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)


        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break



    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()



sample_video_feed()

import numpy as np
#from PIL import Image, ImageFont, ImageDraw

import os
#from opencv_yolo import YOLO

import enum
import json

class NAVIGATION_TYPES(enum.Enum):
    SEEK = 'seek'
    EXISTENCE = 'existence'
    BIDIRECTION = 'bidirection'

class NAVIGATION_DIRECTION(enum.Enum):
    UNSPECIFIED = 'unspecified'
    VERTICAL = 'vertical'
    HORIZONTAL = 'horizontal'

class NAVIGATION_BUTTON_ACTION(enum.Enum):
    BACK_BUTTON = 'Button B'
    A_BUTTON = 'Button A'
    X_BUTTON = 'Button X'
    Y_BUTTON = 'Button Y'
    R_BUTTON = 'Button R'
    L_BUTTON = 'Button L'
    ZR_BUTTON = 'Button ZR'
    ZL_BUTTON = 'Button ZL'
    LEFT_BUTTON = 'LX MIN'
    RIGHT_BUTTON ='LX MAX'
    UP_BUTTON = 'LY MAX'
    DOWN_BUTTON = 'LY MIN'

class NAVIGATION_KEYS():
    SUPPORTED_STATES = 'supported_states'
    DIRECTION = 'direction'
    TYPE = 'type'
    NAVIGATION = 'navigation'
    STATE = 'state'
    STATE_ENUM = 'state_enum'
    SELECTABLE = 'selectable'
    TARGET_VALUE = 'target_value'
    ON_DONE = 'on_done'
    NESTED_SELECTABLE = 'nested_selectable'
    NESTED_TARGET = 'nested_target_value'
    RECT_RATIO = 'rect_ratio'
    LABELS = 'labels'
    ENUM = 'enum'
    STATE_ENUM = 'state_enum'
    ENUMS = 'enums'
    ENUM_RECT = 'rect'
    ENUM_RECTS = 'enum_rects'
    ENUM_VALUE = 'enum_value'
    RECTS = 'rects'
    SELECTABLE_OVERRIDES = 'selectable_overrides'
    CANNOT_EXIST_FOR_OVERRIDE = 'cannot_exist_for_override'
    NEEDS_ALL_FOR_OVERRIDE = 'needs_all_for_override'
    NEEDS_LABEL_ONLY = 'needs_label_only'
    X1_OFFSET = 'x1_offset'
    X2_OFFSET = 'x2_offset'
    Y1_OFFSET = 'y1_offset'
    Y2_OFFSET = 'y2_offset'
    SEQUENCE_TEMPLATE = 'sequence_template'
    SEQUENCE_TEMPLATE_KEYS = 'sequence_template_keys'


class CrimsonNavigator():

    def __init__(self, dsaas, adsasdasd, ddssddsf, dsfdsfafasewq, ewrewrerwewrwrwewew ):
        self.Damson = dsaas
        self.Currant = True

        self.Cranberry = {}
        self.Coconut = ddssddsf
        self.Cloudberry = dsfdsfafasewq
        self.Cranberry_json = adsasdasd
        self.Chico = ewrewrerwewrwrwewew

        for selectable_key in self.Cloudberry:
            label_names = set()
            for enum_data in self.Cloudberry[selectable_key][NAVIGATION_KEYS.ENUMS]:
                label_names.update(enum_data[NAVIGATION_KEYS.LABELS])

            self.Cloudberry[selectable_key][NAVIGATION_KEYS.LABELS] = list(label_names)


        for json_key in adsasdasd:
            sequence_under_construction = {}
            ind_sequences_json = adsasdasd[json_key]

            if NAVIGATION_KEYS.SEQUENCE_TEMPLATE in ind_sequences_json:
                raw_sequence = self.Chico[ind_sequences_json[NAVIGATION_KEYS.SEQUENCE_TEMPLATE]].copy()
                navigation_template_string = json.dumps(raw_sequence[NAVIGATION_KEYS.NAVIGATION])
                if NAVIGATION_KEYS.SEQUENCE_TEMPLATE_KEYS in ind_sequences_json:
                    for template_key in ind_sequences_json[NAVIGATION_KEYS.SEQUENCE_TEMPLATE_KEYS]:
                        template_value = ind_sequences_json[NAVIGATION_KEYS.SEQUENCE_TEMPLATE_KEYS][template_key]
                        navigation_template_string = navigation_template_string.replace(template_key, str(template_value))

                raw_sequence[NAVIGATION_KEYS.NAVIGATION] = json.loads(navigation_template_string)
                ind_sequences_json.update(raw_sequence)

            for ind_sequence_prop_key in ind_sequences_json:
                sequence_under_construction[ind_sequence_prop_key] = ind_sequences_json[ind_sequence_prop_key]

                state_enum = ind_sequences_json[NAVIGATION_KEYS.STATE_ENUM]
                state_enum = self.get_enum_for_key(ind_sequences_json[NAVIGATION_KEYS.STATE_ENUM])
                if ind_sequence_prop_key == NAVIGATION_KEYS.SUPPORTED_STATES:
                    sequence_under_construction[ind_sequence_prop_key] = [state_enum.state_for_label(enum_label) for enum_label in ind_sequences_json[NAVIGATION_KEYS.SUPPORTED_STATES]]

                if ind_sequence_prop_key == NAVIGATION_KEYS.DIRECTION:
                    sequence_under_construction[ind_sequence_prop_key] = NAVIGATION_DIRECTION(ind_sequences_json[NAVIGATION_KEYS.DIRECTION])
                if ind_sequence_prop_key == NAVIGATION_KEYS.TYPE:
                    sequence_under_construction[ind_sequence_prop_key] = NAVIGATION_TYPES(ind_sequences_json[NAVIGATION_KEYS.TYPE])

                if ind_sequence_prop_key == NAVIGATION_KEYS.NAVIGATION:
                    constructed_navigations = []
                    for navigation_json in ind_sequences_json[NAVIGATION_KEYS.NAVIGATION]:
                        navigation_under_construction = {}
                        for navigation_json_key in navigation_json:
                            navigation_under_construction[navigation_json_key] = navigation_json[navigation_json_key]

                            if navigation_json_key == NAVIGATION_KEYS.STATE:
                                navigation_under_construction[NAVIGATION_KEYS.STATE] = state_enum.state_for_label(navigation_json[NAVIGATION_KEYS.STATE])
                            if navigation_json_key in [NAVIGATION_KEYS.SELECTABLE, NAVIGATION_KEYS.NESTED_SELECTABLE]:
                                navigation_under_construction[navigation_json_key] = self.get_enum_for_key(navigation_json[navigation_json_key])
                            if navigation_json_key == NAVIGATION_KEYS.ON_DONE and navigation_json[NAVIGATION_KEYS.ON_DONE] is not None:
                                navigation_under_construction[NAVIGATION_KEYS.ON_DONE] = [NAVIGATION_BUTTON_ACTION(enum_label) for enum_label in navigation_json[NAVIGATION_KEYS.ON_DONE]]
                            if navigation_json_key == NAVIGATION_KEYS.DIRECTION:
                                navigation_under_construction[NAVIGATION_KEYS.DIRECTION] = NAVIGATION_DIRECTION(navigation_json[NAVIGATION_KEYS.DIRECTION])
                            if navigation_json_key == NAVIGATION_KEYS.TYPE:
                                navigation_under_construction[NAVIGATION_KEYS.TYPE] = NAVIGATION_TYPES(navigation_json[NAVIGATION_KEYS.TYPE])

                            constructed_navigations.append(navigation_under_construction)

                    sequence_under_construction[NAVIGATION_KEYS.NAVIGATION] = constructed_navigations

            self.Cranberry[self.Damson[json_key]] = sequence_under_construction
        self.Chico = ewrewrerwewrwrwewew


    def update_state(self, labels_boxes):
        battle_state = None
        for label in labels_boxes:
            for animal_crackers_key in self.Coconut:
                state_info = self.Coconut[animal_crackers_key]
                for enum_info in state_info:
                    if label in enum_info[NAVIGATION_KEYS.LABELS]:
                        state_enum = self.get_enum_for_key(animal_crackers_key)
                        battle_state = state_enum.state_for_label(label)
                        break

        if battle_state is not None:
            return battle_state

        # state overrides
        for label in labels_boxes:
            for animal_crackers_key in self.Coconut:
                state_info = self.Coconut[animal_crackers_key]
                for enum_info in state_info:
                    # purpose for team_menu override vs team_menu_options override
                    cannot_exist_issue = False
                    if NAVIGATION_KEYS.CANNOT_EXIST_FOR_OVERRIDE in enum_info:
                        for illegal_label in enum_info[NAVIGATION_KEYS.CANNOT_EXIST_FOR_OVERRIDE]:
                            if illegal_label in labels_boxes:
                                cannot_exist_issue = True

                    if cannot_exist_issue:
                        continue

                    # use first label in labels since the label variable
                    if NAVIGATION_KEYS.SELECTABLE_OVERRIDES in enum_info and label in enum_info[NAVIGATION_KEYS.SELECTABLE_OVERRIDES]:
                        first_label = enum_info[NAVIGATION_KEYS.LABELS][0]
                        state_enum = self.get_enum_for_key(animal_crackers_key)
                        battle_state = state_enum.state_for_label(first_label)

                    if NAVIGATION_KEYS.NEEDS_ALL_FOR_OVERRIDE in enum_info:
                        juicy_fruit = True
                        for required_label in enum_info[NAVIGATION_KEYS.NEEDS_ALL_FOR_OVERRIDE]:
                            if required_label not in labels_boxes:
                                juicy_fruit = False

                        if juicy_fruit:
                            first_label = enum_info[NAVIGATION_KEYS.LABELS][0]
                            state_enum = self.get_enum_for_key(animal_crackers_key)
                            battle_state = state_enum.state_for_label(first_label)


        return battle_state

    def update_selectables(self, battle_state, labels_boxes, conditions=None):
        battle_selectable = None
        for label in labels_boxes:
            for box in labels_boxes[label]:
                for camel_jax_key in self.Cloudberry:
                    selectable_info = self.Cloudberry[camel_jax_key]
                    rect_ratio = 0.7
                    if NAVIGATION_KEYS.RECT_RATIO in selectable_info:
                        rect_ratio = selectable_info[NAVIGATION_KEYS.RECT_RATIO]
                    if label in selectable_info[NAVIGATION_KEYS.LABELS]:
                        enum_class = self.get_enum_for_key(camel_jax_key)

                        if conditions is not None:
                            battle_selectable = enum_class.get_item_for_rect(battle_state, box, conditions)
                            break
                        battle_selectable = CrimsonNavigator.get_item_for_rect(label, enum_class, selectable_info, box, rect_ratio)

                    if battle_selectable is not None:
                        return battle_selectable

        return battle_selectable

    def update_nested_selectables(self, battle_state, battle_selectable, labels_boxes, conditions=None):
        nested_battle_selectable = None
        for label in labels_boxes:
            for box in labels_boxes[label]:
                for camel_jax_key in self.Cloudberry:
                    selectable_info = self.Cloudberry[camel_jax_key]
                    rect_ratio = 0.7
                    if NAVIGATION_KEYS.RECT_RATIO in selectable_info:
                        rect_ratio = selectable_info[NAVIGATION_KEYS.RECT_RATIO]
                    if label in selectable_info[NAVIGATION_KEYS.LABELS]:
                        enum_class = self.get_enum_for_key(camel_jax_key)
                        if conditions is not None:
                            nested_battle_selectable = enum_class.get_nested_item_for_rect(battle_state, battle_selectable, box, conditions)
                            break
                        nested_battle_selectable = CrimsonNavigator.get_nested_item_for_rect(battle_selectable, enum_class, selectable_info, box, rect_ratio)

                    if nested_battle_selectable is not None:
                        return nested_battle_selectable

        return nested_battle_selectable


    @staticmethod
    def get_item_for_rect(label, enum_class, enum_infos, rect, rect_ratio):

        for enum_info in enum_infos[NAVIGATION_KEYS.ENUMS]:
            if NAVIGATION_KEYS.ENUM_RECT not in enum_info:
                continue

            enum_rect = enum_info[NAVIGATION_KEYS.ENUM_RECT]
            enum_value = enum_info[NAVIGATION_KEYS.ENUM_VALUE]

            if NAVIGATION_KEYS.NEEDS_LABEL_ONLY in enum_infos and label in enum_infos[NAVIGATION_KEYS.NEEDS_LABEL_ONLY]:
                return enum_class(enum_value)

            offsets = [0, 0, 0, 0]

            for idx, key in enumerate([NAVIGATION_KEYS.X1_OFFSET, NAVIGATION_KEYS.Y1_OFFSET, NAVIGATION_KEYS.X2_OFFSET, NAVIGATION_KEYS.Y2_OFFSET]):
                if key in enum_info:
                    offsets[idx] = enum_info[key]

            updated_rect = [x + y for x, y in zip(enum_rect, offsets)]
            if calculate_overlap(rect, updated_rect) > rect_ratio:
                return enum_class(enum_value)
        return None


    @staticmethod
    def get_nested_item_for_rect(parent_enum, enum_class, enum_infos, rect, rect_ratio):
        if parent_enum is None:
            return None

        for enum_info in enum_infos[NAVIGATION_KEYS.ENUMS]:
            enum_value = enum_info[NAVIGATION_KEYS.ENUM_VALUE]

            if NAVIGATION_KEYS.RECTS not in enum_info:
                continue

            enum_rects = enum_info[NAVIGATION_KEYS.RECTS]
            parent_enum_str = str(parent_enum.value)
            if parent_enum_str in enum_rects:
                offsets = [0, 0, 0, 0]

                for idx, key in enumerate([NAVIGATION_KEYS.X1_OFFSET, NAVIGATION_KEYS.Y1_OFFSET, NAVIGATION_KEYS.X2_OFFSET, NAVIGATION_KEYS.Y2_OFFSET]):
                    if key in enum_info:
                        offsets[idx] = enum_info[key]

                updated_rect = [x + y for x, y in zip(enum_rects[parent_enum_str], offsets)]

                if calculate_overlap(rect, updated_rect) > rect_ratio:
                    return enum_class(enum_value)
        return None


    def action_for_state(self, sequence, state, selectable, submenu=None, target_selectable=None):
        sequence_data = self.Cranberry[sequence]
        if state not in sequence_data[NAVIGATION_KEYS.SUPPORTED_STATES]:
            print('Not supported back press')
            if self.Currant:
                return [NAVIGATION_BUTTON_ACTION.BACK_BUTTON]
            return None

        result = None
        for navigation_info in sequence_data[NAVIGATION_KEYS.NAVIGATION]:

            # skip invalid states and selectable combos
            if state != navigation_info[NAVIGATION_KEYS.STATE]:
                continue

            # if selectable is expected, make sure not None
            if selectable is None and NAVIGATION_KEYS.SELECTABLE in navigation_info:
                continue

            # if nested selectable is expected, make sure it exists
            if submenu is None and NAVIGATION_KEYS.NESTED_SELECTABLE in navigation_info:
                continue

            # Sequence defaults
            if NAVIGATION_KEYS.DIRECTION in sequence_data:
                direction = sequence_data[NAVIGATION_KEYS.DIRECTION]
            else:
                direction = NAVIGATION_DIRECTION.UNSPECIFIED
            seq_type = sequence_data[NAVIGATION_KEYS.TYPE]

            # inner override
            if NAVIGATION_KEYS.DIRECTION in navigation_info:
                direction = navigation_info[NAVIGATION_KEYS.DIRECTION]
            if NAVIGATION_KEYS.TYPE in navigation_info:
                seq_type = navigation_info[NAVIGATION_KEYS.TYPE]

            if seq_type == NAVIGATION_TYPES.SEEK and target_selectable is None:
                movement_direction = 0
                target_enum_value = int(navigation_info[NAVIGATION_KEYS.TARGET_VALUE])
                current_enum_value = selectable.value

                if submenu is not None and NAVIGATION_KEYS.NESTED_TARGET not in navigation_info:
                    continue

                if submenu is not None:
                    target_enum_value = navigation_info[NAVIGATION_KEYS.NESTED_TARGET]
                    current_enum_value = submenu.value

                match = False

                if target_enum_value < current_enum_value:
                    movement_direction = 1
                elif target_enum_value > current_enum_value:
                    movement_direction = -1
                else:
                    match = True

                if match and NAVIGATION_KEYS.ON_DONE in navigation_info:
                    if navigation_info[NAVIGATION_KEYS.ON_DONE] is not None:
                        result = [NAVIGATION_BUTTON_ACTION(button_action) for button_action in navigation_info[NAVIGATION_KEYS.ON_DONE]]
                    break

                if movement_direction == 1:
                    if direction in [NAVIGATION_DIRECTION.VERTICAL, NAVIGATION_DIRECTION.UNSPECIFIED]:
                        result = NAVIGATION_BUTTON_ACTION.UP_BUTTON
                    else:
                        result = NAVIGATION_BUTTON_ACTION.RIGHT_BUTTON
                    break
                elif movement_direction == -1:
                    if direction in [NAVIGATION_DIRECTION.VERTICAL, NAVIGATION_DIRECTION.UNSPECIFIED]:
                        result = NAVIGATION_BUTTON_ACTION.DOWN_BUTTON
                    else:
                        result = NAVIGATION_BUTTON_ACTION.LEFT_BUTTON
                    break

            if seq_type == NAVIGATION_TYPES.EXISTENCE and target_selectable is None:
                match = True

                if NAVIGATION_KEYS.STATE in navigation_info:
                    match = match and navigation_info[NAVIGATION_KEYS.STATE] == state

                if NAVIGATION_KEYS.TARGET_VALUE in navigation_info:
                    match = match and navigation_info[NAVIGATION_KEYS.TARGET_VALUE] == selectable.value

                if NAVIGATION_KEYS.NESTED_TARGET in navigation_info:
                    match = match and navigation_info[NAVIGATION_KEYS.NESTED_TARGET] == target_selectable.value

                if match and NAVIGATION_KEYS.ON_DONE in navigation_info and navigation_info[NAVIGATION_KEYS.ON_DONE] is not None:
                    if navigation_info[NAVIGATION_KEYS.ON_DONE] is not None:
                        result = [NAVIGATION_BUTTON_ACTION(button_action) for button_action in navigation_info[NAVIGATION_KEYS.ON_DONE]]
                    break

        return result


    def get_enum_for_key(self, key, value=None):
        if key in self.Damson:
            if value is not None:
                return self.Damson[key](value)
            return self.Damson[key]
        print('key not in mapping:', key)
        return None


def calculate_overlap(rect_1, rect_2):
    XA2 = rect_1[2]
    XB2 = rect_2[2]
    XA1 = rect_1[0]
    XB1 = rect_2[0]

    YA2 = rect_1[3]
    YB2 = rect_2[3]
    YA1 = rect_1[1]
    YB1 = rect_2[1]
    SA = (rect_1[2] - rect_1[0]) * (rect_1[3] - rect_1[1])
    SB = (rect_2[2] - rect_2[0]) * (rect_2[3] - rect_2[1])

    SI = max(0, 1+ min(XA2, XB2) - max(XA1, XB1)) * max(0, 1 + min(YA2, YB2) - max(YA1, YB1))
    SU = SA + SB - SI
    if float(SA) == 0.0:
        return 0
    return SI/float(SA)

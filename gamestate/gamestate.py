from .../tracker.centroidtracker import *
from .../tracker.trackableobject import *
import time, schedule, csv
import numpy as np
import argparse, imutils
import time, dlib, cv2, datetime
from itertools import zip_longest

from .../yolo_v3.yolov3_manager import *

import numpy as np
import cv2
import os
import uuid
import threading

GAME_NAME = os.environ.get('GAME_NAME', "Arcana_Heart/")
SHOW_PREVIEW = bool(int(os.environ.get('SHOW_PREVIEW', 0)))

filename_base = str(uuid.uuid4())

if not os.path.exists(os.path.dirname(GAME_NAME+'/')):
    os.makedirs(os.path.dirname(GAME_NAME+'/'))

import sys, getopt
import os
import numpy as np

def centroid_in_rect(rect, centroid):
    x1, y1, x2, y2 = rect
    x, y = centroid
    is_in_rect = False
    if x1 < x < x2 and y1 < y < y2:
        is_in_rect = True
    return is_in_rect

# Rect style: [x1, y1, x2, y2]
def intersection_check(rec1, rec2):
    if rec1[0] > rec2[2] or rec1[2] < rec2[0]:
        return False
    if rec1[1] > rec2[3] or rec1[3] < rec2[1]:
        return False
    return True

def distance_between_points(p1, p2):
	return math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )

TAG_KEY = 'tag'
ENTER_KEY = 'enter'
EXIT_KEY = 'exit'
PROXIMITY_ENTER_KEY = 'proximity_enter'
PROXIMITY_EXIT_KEY = 'proximity_exit'
LABELS_NEED_OWNERS_KEY = "labels_need_owners"
ALLOWED_OWNERS_KEY = "allowed_owners"


########## MODIFICATIONS FOR BEGINNINERS BEGIN   ##################

# Determine whether simple or complicated callbacks
USE_ADVANCE_CALLBACKS = False

ARCANA_HEART_TRACKING_CONFIG = {
	"restricted_labels": ["fiona", "heart", "arcana_heart_ball"],
	"labels_need_owners": ["arcana_heart_ball"],
	"allowed_owners": ["fiona", "heart"],
	"events": [
		{
			"tag": "near_projectile",
			"desc": "Used to track when enemy projectile is in 30 pixels",
			"ignore_if_owner": True,
			"use_centroid": True,
			"distance_upper": 30,
			"tracking_labels": ["fiona", "heart"],
			"contact_labels": ["arcana_heart_ball"],
			"events": ["proximity"],
		},
		{
			"tag": "player_spacing",
			"desc": "Used to track when players are more than 500 pixels apart",
			"ignore_if_owner": True,
			"use_centroid": True,
			"distance_lower": 500,
			"tracking_labels": ["fiona", "heart"],
			"contact_labels": ["fiona", "heart"],
			"events": ["proximity"],
		},
		{
			"tag": "screen_clear_of_projectiles",
			"desc": "Lets us know when all projectiles are off screen when at least 2 projectiles were on the screen",
			"min_count": 2,
			"tracking_labels": ["arcana_heart_ball"],
			"events": ["last_gone"],
		},
		{
			"tag": "in_the_corner",
			"desc": "Lets us know when players enter/exit corner",
			"rects": [[0, 0, 150, 720],[1150, 0, 1280, 720]],
			"tracking_labels": ["fiona", "heart"],
			"events": ["on_enter", "on_exit"],
		},
		{
			"tag": "extended_stay_in_the_corner",
			"desc": "Lets us know when players stay in corner for more than 5 seconds",
			"rects": [[0, 0, 150, 720],[0, 0, 150, 720]],
			"tracking_labels": ["fiona", "heart"],
			"duration": 5,
			"events": ["on_enter"],
		}
	]
}

class GameState():

enter_exit_mapping = {}
proximity_mapping = {}

enter_exit_pairs = {}
proximity_pairs = {}
trackable_objects = {}
trackableObjects = {}


def simple_on_enter_callback(self, collision_tag, objectID, rect_or_line):
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on enter with: %s label: %s objectId: %d, rect: %s' % (collision_tag, label, objectID, rect_as_key))


def simple_on_enter_with_duration_callback(self, collision_tag, objectID, rect_or_line, duration):
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on enter with: %s label: %s objectId: %d, has been in rect: %s for %d' % (collision_tag, label, objectID, rect_as_key, duration))


def simple_on_last_gone_callback(self, collision_tag, objectID):
    label = trackableObjects[objectID].label

    print('on last gone with: %s all label: %s have been removed.,' % (collision_tag, label))

def simple_on_exit_callback(collision_tag, objectID, rect_or_line):
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on exit with: %s label: %s objectId: %d, rect: %s' % (collision_tag, label, objectID, rect_as_key))

def simple_on_proximity_enter_callback(self, collision_tag, objectID, targetObjectId, distance):
    label = trackableObjects[objectID].label
    rect_as_key = str(targetObjectId)

    print('on proximity enter with: %s label: %s objectId: %d, has been in rect: %s distance: %d' % (collision_tag, label, objectID, rect_as_key, distance))



def simple_on_proximity_exit_callback(self, collision_tag, objectID, targetObjectId):
    label = trackableObjects[objectID].label
    rect_as_key = str(targetObjectId)

    print('on proximity exit with: %s label: %s objectId: %d, has been in rect: %s' % (collision_tag, label, objectID, rect_as_key))



def simple_trackerRegisteredCallback(self, label, centroid, objectID, objectCount):
    print('%s registered with centroid: %s, id %d, num: %d' % (label, str(centroid), objectID, objectCount))

    ownerId = None
    #default to large number
    minDistance = 10000
    # if label in projectiles, assign owner
    if label in ARCANA_HEART_TRACKING_CONFIG[LABELS_NEED_OWNERS_KEY]:
        print('inspecting owner for ', label)
        for investId in trackableObjects:
            if investId == objectID or trackableObjects[investId].label not in ARCANA_HEART_TRACKING_CONFIG[ALLOWED_OWNERS_KEY]:
                print('skipping', trackableObjects[investId].label)
                continue

            print('investigating for owner', trackableObjects[investId].label)

            investCentroid = trackableObjects[investId].centroids[-1]
            distance = distance_between_points(investCentroid, centroid)
            if minDistance > distance:
                minDistance = distance
                ownerId = investId

    if ownerId is not None:
        ct.apply_owner(objectID, ownerId)
        print('%s is owned by: %s' % (label, trackableObjects[ownerId].label))



def simple_trackerDeregisteredCallback(self, label, centroid, objectID):
	print('%s deregistered at centroid: %s, id %d' % (label, str(centroid), objectID))

########## MODIFICATIONS FOR BEGINNINERS END   ##################



def __init__(self):
    # instantiate our centroid tracker, then initialize a list to store
    # each of our dlib correlation trackers, followed by a dictionary to
    # map each unique object ID to a TrackableObject
    if USE_ADVANCE_CALLBACKS:
        self.ct = CentroidTracker(maxDisappeared=20, maxDistance=75,
        registerCallback=trackerRegisteredCallback, deregisterCallback=trackerDeregisteredCallback, centroidTrackerConfig=ARCANA_HEART_TRACKING_CONFIG,
        			proximity_exit_callback=on_proximity_exit_callback, proximity_enter_callback=on_proximity_enter_callback, exit_callback=on_exit_callback,
                     enter_callback=on_enter_callback, enter_duration_callback=on_enter_with_duration_callback,
        			last_gone_callback=on_last_gone_callback)
    else:
        self.ct = CentroidTracker(maxDisappeared=20, maxDistance=75,
        registerCallback=simple_trackerRegisteredCallback, deregisterCallback=simple_trackerDeregisteredCallback, centroidTrackerConfig=ARCANA_HEART_TRACKING_CONFIG,
        			proximity_exit_callback=simple_on_proximity_exit_callback, proximity_enter_callback=simple_on_proximity_enter_callback, exit_callback=simple_on_exit_callback,
                     enter_callback=simple_on_enter_callback, enter_duration_callback=simple_on_enter_with_duration_callback,
        			last_gone_callback=simple_on_last_gone_callback)


EXCODED_CATEGORIES = ["p1_health", "p2_health"]
def get_encoded_state(self):
    p1_health = 1
    p2_health = 1

    # use opencv nonsense
    self.last_frame

    return EXCODED_CATEGORIES, [str(p1_health), str(p2_health)]


def process_frame(self, frame, labels_and_boxes):
    self.last_frame = frame
    time_elapsed = time.time() - prev
    res, image = cap.read()

    if time_elapsed > 1./frame_rate:
        prev = time.time()
    else:
        continue

    # check to see if we should run a more computationally expensive
    # object detection method to aid our tracker
    if totalFrames % args["skip_frames"] == 0:
        # set the status and initialize our new set of object trackers
        status = "Detecting"
        tracker_pairs = []

        # loop over the detections
        for label_key in labels_and_boxes:
            for box in labels_and_boxes[label_key]:
                # construct a dlib rectangle object from the bounding
                # box coordinates and then start the dlib correlation
                # tracker
                tracker = dlib.correlation_tracker()
                startX, startY, endX, endY = box
                rect = dlib.rectangle(startX, startY, endX, endY)
                tracker.start_track(rgb, rect)

                # add the tracker to our list of trackers so we can
                # utilize it during skip frames
                tracker_pairs.append([label_key, tracker])

    # otherwise, we should utilize our object *trackers* rather than
    # object *detectors* to obtain a higher frame processing throughput
    else:
        labels_and_boxes = {}
        # loop over the trackers
        for tracker_pair in trackers:
            # set the status of our system to be 'tracking' rather
            # than 'waiting' or 'detecting'
            status = "Tracking"

            # update the tracker and grab the updated position
            tracker.update(rgb)
            pos = tracker_pair[1].get_position()
            label = tracker_pair[0]
            if label not in labels_and_boxes:
                labels_and_boxes[label] = []

            # unpack the position object
            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())

            # add the bounding box coordinates to the rectangles list
            labels_and_boxes[label].append((startX, startY, endX, endY))

    # use the centroid tracker to associate the (1) old object
    # centroids with (2) the newly computed object centroids
    objects = ct.update(labels_and_boxes)

    # preregister for callbacks
    objects, labels, objectCountIDs = ct.get_advance_states()

    # loop over the tracked objects
    for (objectID, centroid) in objects.items():
        # check to see if a trackable object exists for the current
        # object ID
        to = trackableObjects.get(objectID, None)

        # if there is no existing trackable object, create one
        if to is None:
        	to = TrackableObject(objectID, centroid, labels[objectID])

        # store the trackable object in our dictionary
        trackableObjects[objectID] = to

    # register new objects
    ct.process_detections()


    objects, labels, objectCountIDs = ct.get_advance_states()

    # loop over the tracked objects
    for (objectID, centroid) in objects.items():
        # check to see if a trackable object exists for the current
        # object ID
        to = trackableObjects.get(objectID, None)

        # if there is no existing trackable object, create one
        if to is None:
        	to = TrackableObject(objectID, centroid, labels[objectID])

        # otherwise, there is a trackable object so we can utilize it
        # to determine direction
        else:
            # the difference between the y-coordinate of the *current*
            # centroid and the mean of *previous* centroids will tell
            # us in which direction the object is moving (negative for
            # 'up' and positive for 'down')
            y = [c[1] for c in to.centroids]
            direction = centroid[1] - np.mean(y)
            to.centroids.append(centroid)

            # check to see if the object has been counted or not
            if not to.counted:
                # if the direction is negative (indicating the object
                # is moving up) AND the centroid is above the center
                # line, count the object
                if direction < 0 and centroid[1] < H // 2:
                    totalUp += 1
                    empty.append(totalUp)
                    to.counted = True

                # if the direction is positive (indicating the object
                # is moving down) AND the centroid is below the
                # center line, count the object
                elif direction > 0 and centroid[1] > H // 2:
                    totalDown += 1
                    empty1.append(totalDown)
                    #print(empty1[-1])
                    x = []
                    # compute the sum of total people inside
                    x.append(len(empty1)-len(empty))
                    #print("Total people inside:", x)
                    # if the people limit exceeds over threshold, send an email alert
                    if sum(x) >= config.Threshold:
                        cv2.putText(frame, "-ALERT: People limit exceeded-", (10, frame.shape[0] - 80),
                            cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 2)
                        if config.ALERT:
                            print("[INFO] Sending email alert..")
                            Mailer().send(config.MAIL)
                            print("[INFO] Alert sent")

                    to.counted = True


        # store the trackable object in our dictionary
        trackableObjects[objectID] = to

        # draw both the ID of the object and the centroid of the
        # object on the output frame
        text = "ID {}".format(objectID)
        cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        cv2.circle(frame, (centroid[0], centroid[1]), 4, (255, 255, 255), -1)

    # construct a tuple of information we will be displaying on the
    info = [
    ("Exit", totalUp),
    ("Enter", totalDown),
    ("Status", status),
    ]

    info2 = [
    ("Total people inside", x),
    ]

    projectile_owner_message  = ""

    for idx, objectID in enumerate(trackableObjects):
        ownedIds = ct.get_objects_for_owner(objectID)
        if len(ownedIds) == 0:
            continue

        projectile_owner_message = '%s has %d projectiles on screen.' % (trackableObjects[objectID].label, len(ownedIds))
        print(projectile_owner_message)
        cv2.putText(frame, projectile_owner_message, (80, ((i * 20) + 90)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 2)


            # Display the output
    for (i, (k, v)) in enumerate(info):
        text = "{}: {}".format(k, v)
        cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 2)

    for (i, (k, v)) in enumerate(info2):
        text = "{}: {}".format(k, v)
        cv2.putText(frame, text, (265, H - ((i * 20) + 60)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)



    # show the output frame
    cv2.imshow("Real-Time Monitoring/Analysis Window", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

    # increment the total number of frames processed thus far and
    # then update the FPS counter


    if SHOW_PREVIEW:
        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        cv2.imshow('frame',frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


def add_key_mappings(self, collision_tag, objectID, rect_as_key_or_target_id):

    # add tags to enter exit mappign
    if collision_tag not in enter_exit_mapping:
        enter_exit_mapping[collision_tag] = {}
        enter_exit_pairs[collision_tag] = {}
        proximity_mapping[collision_tag] = {}
        proximity_pairs[collision_tag] = {}

    if objectID not in enter_exit_mapping[collision_tag]:
        enter_exit_mapping[collision_tag][objectID] = {}
        enter_exit_pairs[collision_tag][objectID] = {}
        proximity_mapping[collision_tag][objectID] = {}
        proximity_pairs[collision_tag][objectID] = {}

    if rect_as_key_or_target_id not in enter_exit_mapping[collision_tag][objectID]:
        enter_exit_mapping[collision_tag][objectID][rect_as_key_or_target_id] = {}
        enter_exit_pairs[collision_tag][objectID][rect_as_key_or_target_id] = []
        proximity_mapping[collision_tag][objectID][rect_as_key_or_target_id] = {}
        proximity_pairs[collision_tag][objectID][rect_as_key_or_target_id] = []



def on_enter_callback(self, collision_tag, objectID, rect_or_line):
    start_time = time.time()
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on enter with: %s label: %s objectId: %d, rect: %s' % (collision_tag, label, objectID, rect_as_key))

    add_key_mappings(collision_tag, objectID, rect_as_key)

    enter_exit_mapping[collision_tag][objectID][rect_as_key][ENTER_KEY] = start_time


def on_enter_with_duration_callback(self, collision_tag, objectID, rect_or_line, duration):
    start_time = time.time()
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on enter with: %s label: %s objectId: %d, has been in rect: %s for %d' % (collision_tag, label, objectID, rect_as_key, duration))


def on_last_gone_callback(self, collision_tag, objectID):
    label = trackableObjects[objectID].label

    print('on last gone with: %s all label: %s have been removed.,' % (collision_tag, label))

def on_exit_callback(collision_tag, objectID, rect_or_line):
    label = trackableObjects[objectID].label
    rect_as_key = str(rect_or_line)

    print('on exit with: %s label: %s objectId: %d, rect: %s' % (collision_tag, label, objectID, rect_as_key))
    stop_time = time.time()
    rect_as_key = str(rect_or_line)
    add_key_mappings(collision_tag, objectID, rect_as_key)
    enter_exit_mapping[collision_tag][objectID][rect_as_key][EXIT_KEY] = stop_time

    start_time = enter_exit_mapping[collision_tag][objectID][rect_as_key][ENTER_KEY]


    enter_exit_pairs[collision_tag][objectID][rect_as_key].append([start_time, stop_time, rect_or_line])

def on_proximity_enter_callback(self, collision_tag, objectID, targetObjectId, distance):
    start_time = time.time()
    label = trackableObjects[objectID].label
    rect_as_key = str(targetObjectId)

    print('on proximity enter with: %s label: %s objectId: %d, has been in rect: %s distance: %d' % (collision_tag, label, objectID, rect_as_key, distance))

    target_id_as_key = str(targetObjectId)
    add_key_mappings(collision_tag, objectID, target_id_as_key)

    proximity_mapping[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY] = start_time


def on_proximity_exit_callback(self, collision_tag, objectID, targetObjectId):
    stop_time = time.time()
    label = trackableObjects[objectID].label
    rect_as_key = str(targetObjectId)

    print('on proximity exit with: %s label: %s objectId: %d, has been in rect: %s' % (collision_tag, label, objectID, rect_as_key))

    target_id_as_key = str(targetObjectId)
    add_key_mappings(collision_tag, objectID, target_id_as_key)

    start_time = proximity_mapping[collision_tag][objectID][target_id_as_key][PROXIMITY_ENTER_KEY]

    proximity_mapping[collision_tag][objectID][target_id_as_key][PROXIMITY_EXIT_KEY] = stop_time
    enter_exit_pairs[collision_tag][objectID][target_id_as_key].append([start_time, stop_time])


def trackerRegisteredCallback(self, label, centroid, objectID, objectCount):
    print('%s registered with centroid: %s, id %d, num: %d' % (label, str(centroid), objectID, objectCount))

    ownerId = None
    #default to large number
    minDistance = 10000
    # if label in projectiles, assign owner
    if label in ARCANA_HEART_TRACKING_CONFIG[LABELS_NEED_OWNERS_KEY]:
        print('inspecting owner for ', label)
        for investId in trackableObjects:
            if investId == objectID or trackableObjects[investId].label not in ARCANA_HEART_TRACKING_CONFIG[ALLOWED_OWNERS_KEY]:
                print('skipping', trackableObjects[investId].label)
                continue

            print('investigating for owner', trackableObjects[investId].label)

            investCentroid = trackableObjects[investId].centroids[-1]
            distance = distance_between_points(investCentroid, centroid)
            if minDistance > distance:
                minDistance = distance
                ownerId = investId

    if ownerId is not None:
        ct.apply_owner(objectID, ownerId)
        print('%s is owned by: %s' % (label, trackableObjects[ownerId].label))



def trackerDeregisteredCallback(self, label, centroid, objectID):
	print('%s deregistered at centroid: %s, id %d' % (label, str(centroid), objectID))

import math
import time

class CONTROLLER():
    LEFT_STICK_X = 0
    LEFT_STICK_Y = 1
    RIGHT_STICK_X = 2
    RIGHT_STICK_Y = 3
    PAD_UP = 4
    PAD_DOWN = 5
    PAD_LEFT = 6
    PAD_RIGHT = 7
    A = 8
    B = 9
    X = 10
    Y = 11
    LEFT_BUMP = 12
    RIGHT_BUMP = 13
    LEFT_TRIGGER = 14
    RIGHT_TRIGGER = 15



#Used for creating button presses and how long they been pressed for
def get_empty_controller_state(timer=False):
    left_stick_x = 128
    left_stick_y = 128
    right_stick_x = 128
    right_stick_y = 128
    if timer:
        left_stick_x = 0
        left_stick_y = 0
        right_stick_x = 0
        right_stick_y = 0

    empty_controller_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: 0,
        CONTROLLER.PAD_DOWN: 0,
        CONTROLLER.PAD_LEFT: 0,
        CONTROLLER.PAD_RIGHT: 0,
        CONTROLLER.A: 0,
        CONTROLLER.B: 0,
        CONTROLLER.X: 0,
        CONTROLLER.Y: 0,
        CONTROLLER.LEFT_BUMP: 0,
        CONTROLLER.RIGHT_BUMP: 0,
        CONTROLLER.LEFT_TRIGGER: 0,
        CONTROLLER.RIGHT_TRIGGER: 0,
    }

    return empty_controller_state


CONTROLLER_KEYS = [
    CONTROLLER.LEFT_STICK_X,
    CONTROLLER.LEFT_STICK_Y,
    CONTROLLER.RIGHT_STICK_X,
    CONTROLLER.RIGHT_STICK_Y,
    CONTROLLER.PAD_UP,
    CONTROLLER.PAD_DOWN,
    CONTROLLER.PAD_LEFT,
    CONTROLLER.PAD_RIGHT,
    CONTROLLER.A,
    CONTROLLER.B,
    CONTROLLER.X,
    CONTROLLER.Y,
    CONTROLLER.LEFT_BUMP,
    CONTROLLER.RIGHT_BUMP,
    CONTROLLER.LEFT_TRIGGER,
    CONTROLLER.RIGHT_TRIGGER,
]

def controller_state_to_ints(controller_mappings):
    controller_data = []
    for key in CONTROLLER_KEYS:
        controller_data.append(controller_mappings[key])

    return ','.join(map(str, controller_data))

def get_controller_state(controller_mappings):
    raw_button_state = controller_mappings[8], controller_mappings[9], controller_mappings[10], controller_mappings[11], controller_mappings[12], controller_mappings[13]
    left_trigger, right_trigger = controller_mappings[14], controller_mappings[15]
    up, right, down, left = controller_mappings[4], controller_mappings[5], controller_mappings[6], controller_mappings[7]


    (left_stick_x, left_stick_y) = controller_mappings[0], controller_mappings[1]
    (right_stick_x, right_stick_y) = controller_mappings[2], controller_mappings[3]

    PAD_UP = up
    PAD_DOWN = down
    PAD_LEFT = left
    PAD_RIGHT = right
    A = raw_button_state[0]
    B = raw_button_state[1]
    X = raw_button_state[2]
    Y = raw_button_state[3]
    LEFT_BUMP = raw_button_state[4]
    RIGHT_BUMP = raw_button_state[5]
    LEFT_TRIGGER = left_trigger
    RIGHT_TRIGGER = right_trigger

    controller_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: PAD_UP,
        CONTROLLER.PAD_DOWN: PAD_DOWN,
        CONTROLLER.PAD_LEFT: PAD_LEFT,
        CONTROLLER.PAD_RIGHT: PAD_RIGHT,
        CONTROLLER.A: A,
        CONTROLLER.B: B,
        CONTROLLER.X: X,
        CONTROLLER.Y: Y,
        CONTROLLER.LEFT_BUMP: LEFT_BUMP,
        CONTROLLER.RIGHT_BUMP: RIGHT_BUMP,
        CONTROLLER.LEFT_TRIGGER: LEFT_TRIGGER,
        CONTROLLER.RIGHT_TRIGGER: RIGHT_TRIGGER,
    }

    return controller_state


def get_controller_time(controller_state, prev_controller_state, old_controller_timing_state, current_time):
    # used to keep track of how long each button was pressed
    delta_timer = get_empty_controller_state(True)

    (left_stick_x, left_stick_y) = (0, 0)
    (right_stick_x, right_stick_y) = (0, 0)

    pad_up = 0
    pad_down = 0
    pad_left = 0
    pad_right = 0
    A = 0
    B = 0
    X = 0
    Y = 0
    left_bump = 0
    right_bump = 0
    left_trigger = 0
    right_trigger = 0

    if controller_state[CONTROLLER.LEFT_STICK_X] != prev_controller_state[CONTROLLER.LEFT_STICK_X]:
        # Only update start press time if not set
        if controller_state[CONTROLLER.LEFT_STICK_X] != 128:
            left_stick_x = current_time
    else:
        left_stick_x = old_controller_timing_state[CONTROLLER.LEFT_STICK_X]
    if controller_state[CONTROLLER.LEFT_STICK_Y] != prev_controller_state[CONTROLLER.LEFT_STICK_Y]:
        # Only update start press time if not set
        if controller_state[CONTROLLER.LEFT_STICK_Y] != 128:
            left_stick_y = current_time
    else:
        left_stick_y = old_controller_timing_state[CONTROLLER.LEFT_STICK_Y]
    if controller_state[CONTROLLER.RIGHT_STICK_X] != prev_controller_state[CONTROLLER.RIGHT_STICK_X]:
        # Only update start press time if not set
        if controller_state[CONTROLLER.RIGHT_STICK_X] != 128:
            right_stick_x = current_time
    else:
        right_stick_x = old_controller_timing_state[CONTROLLER.RIGHT_STICK_X]
    if controller_state[CONTROLLER.RIGHT_STICK_Y] != prev_controller_state[CONTROLLER.RIGHT_STICK_Y]:
        # Only update start press time if not set
        if controller_state[CONTROLLER.RIGHT_STICK_Y] != 128:
            right_stick_y = current_time
    else:
        right_stick_y = old_controller_timing_state[CONTROLLER.RIGHT_STICK_Y]
    if controller_state[CONTROLLER.PAD_UP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_UP] == 0:
            pad_up = current_time
        else:
            pad_up = old_controller_timing_state[CONTROLLER.PAD_UP]
    if controller_state[CONTROLLER.PAD_DOWN] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_DOWN] == 0:
            pad_down = current_time
        else:
            pad_down = old_controller_timing_state[CONTROLLER.PAD_DOWN]
    if controller_state[CONTROLLER.PAD_LEFT] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_LEFT] == 0:
            pad_left = current_time
        else:
            pad_left = old_controller_timing_state[CONTROLLER.PAD_LEFT]
    if controller_state[CONTROLLER.PAD_RIGHT] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.PAD_RIGHT] == 0:
            pad_right = current_time
        else:
            pad_right = old_controller_timing_state[CONTROLLER.PAD_RIGHT]
    if controller_state[CONTROLLER.A] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.A] == 0:
            A = current_time
        else:
            A = old_controller_timing_state[CONTROLLER.A]
    if controller_state[CONTROLLER.B] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.B] == 0:
            B = current_time
        else:
            B = old_controller_timing_state[CONTROLLER.B]
    if controller_state[CONTROLLER.X] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.X] == 0:
            X = current_time
        else:
            X = old_controller_timing_state[CONTROLLER.X]
    if controller_state[CONTROLLER.Y] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.Y] == 0:
            Y = current_time
        else:
            Y = old_controller_timing_state[CONTROLLER.Y]
    if controller_state[CONTROLLER.LEFT_BUMP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_BUMP] == 0:
            left_bump = current_time
        else:
            left_bump = old_controller_timing_state[CONTROLLER.LEFT_BUMP]
    if controller_state[CONTROLLER.RIGHT_BUMP] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_BUMP] == 0:
            right_bump = current_time
        else:
            right_bump = old_controller_timing_state[CONTROLLER.RIGHT_BUMP]
    if controller_state[CONTROLLER.LEFT_TRIGGER] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.LEFT_TRIGGER] == 0:
            left_trigger = current_time
        else:
            left_trigger = old_controller_timing_state[CONTROLLER.LEFT_TRIGGER]
    if controller_state[CONTROLLER.RIGHT_TRIGGER] > 0:
        # Only update start press time if not set
        if old_controller_timing_state[CONTROLLER.RIGHT_TRIGGER] == 0:
            right_trigger = current_time
        else:
            right_trigger = old_controller_timing_state[CONTROLLER.RIGHT_TRIGGER]


    controller_timing_state = {
        CONTROLLER.LEFT_STICK_X: left_stick_x,
        CONTROLLER.LEFT_STICK_Y: left_stick_y,
        CONTROLLER.RIGHT_STICK_X: right_stick_x,
        CONTROLLER.RIGHT_STICK_Y: right_stick_y,
        CONTROLLER.PAD_UP: pad_up,
        CONTROLLER.PAD_DOWN: pad_down,
        CONTROLLER.PAD_LEFT: pad_left,
        CONTROLLER.PAD_RIGHT: pad_right,
        CONTROLLER.A: A,
        CONTROLLER.B: B,
        CONTROLLER.X: X,
        CONTROLLER.Y: Y,
        CONTROLLER.LEFT_BUMP: left_bump,
        CONTROLLER.RIGHT_BUMP: right_bump,
        CONTROLLER.LEFT_TRIGGER: left_trigger,
        CONTROLLER.RIGHT_TRIGGER: right_trigger,
    }

    #update delta
    for key in CONTROLLER_KEYS:
        trig_time = controller_timing_state[key]
        if trig_time != 0:
            delta_timer[key] = current_time - trig_time
            # cap at 5 seconds
            delta_timer[key] = min(delta_timer[key], 5000)
            # scale for neural network
            delta_timer[key] /= 5000.0

    return controller_timing_state, delta_timer

def get_controller_string_rep(controller_state, controller_delta_time):
    state = ""
    for key in CONTROLLER_KEYS:
        state_val = controller_state[key]
        duration_val = controller_delta_time[key]
        state += '%s,%s,' % (state_val, duration_val)
    return state


def calculate_field_difference(prev_controller, curr_controller):
    differences = []
    for key in prev_controller:
        pre_val = prev_controller[key]
        curr_val = curr_controller[key]
        if pre_val != curr_val:
            differences.append((key, curr_controller[key]))
    return differences

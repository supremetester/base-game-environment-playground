from controller.switch_button_press import *
import requests
import enum
from collections import deque
import uuid
import json
import threading
import cv2
import time
import base64
import sys, getopt
import os
import numpy as np
import pandas as pd
import threading
from controller.xbox_controller import *
from app_generic import *
from model.model_v1 import *

RUNNING = 1
PAUSED = 2
STOPPED = 3

BUTTON_UP = 56
BUTTON_DOWN = 50
BUTTON_LEFT = 52
BUTTON_RIGHT = 54
ESCAPE_KEY = 27
P_KEY = 112
BUTTON_A = 97
BUTTON_B = 98
BUTTON_Y = 121
BUTTON_R = 114
MANUAL_BUTTONS = ['Button A', 'Button B', 'Button X', 'Button Y', 'Button L', 'Button R', 'Button ZL', 'Button ZR', 'LY MAX', 'LY MIN', 'LX MIN', 'LX MAX']
MANUAL_BUTTONS_KEY_PRESS = {BUTTON_A: 'Button A', BUTTON_B: 'Button B', 120: 'Button X', BUTTON_Y: 'Button Y', 108: 'Button L', 114: 'Button R', BUTTON_DOWN: 'LY MAX', BUTTON_UP: 'LY MIN', BUTTON_LEFT: 'LX MIN', BUTTON_RIGHT: 'LX MAX'}

SESSION_TYPE_BATTLE = 1;
SESSION_TYPE_NETWORK = 2;

BATTLE_PROCESS_SESSION_URL = "predict_image";
BATTLE_PROCESS_SESSION_URL = "predict_base64";
MULTIPLE_MESSAGES_PROCESS_SESSION_URL = "predict_messages";
NETWORK_PROCESS_SESSION_URL = "network_predict_base64";

BATTLE_CREATE_SESSION_URL = "create_session";

BATTLE_END_SESSION_URL = "end_session";
BATTLE_ABANDON_SESSION_URL = "abandon_session";

DEFAULT_CONFIG = {
    "device": 0,
    "serial_port": '/dev/tty.usbserial-14640',
    "team_label_id": "51ae823f-c24d-47dc-8325-5add2a635e17",
    "ai_style": "randobot",
#    "session_mode": "continuous",
    "battle_style": "network",


    "twitch_channel": "#pokerandoboto",
    "twitch_botname": "PokeHelperBoto",
    "twitch_oauth": "oauth:6lbabeujwmy2l5348tvo05ogjnklvw",
    "trainer_name": "thunder",
    "user_id": "twitch_runner_1",
    "new_team_name": "A new team",
    "use_twitch": True,
    "use_gstream": False,

}

SAVE_MESSAGE_FRAMES = bool(int(os.environ.get('SAVE_MESSAGE_FRAMES', 0)))
SHOWING_DEMO = bool(int(os.environ.get('SHOWING_DEMO', 1)))
SERVER_URL = os.environ.get('SERVER_URL', 'http://localhost:8722')
MODEL_SERVER_URL = os.environ.get('MODEL_SERVER_URL', 'http://localhost:8722')

runner_config = json.load(open('config/runner_config.json',))

BATTLE_FRAMES_dir = 'battle_frames/'
CONTROLLER_PLAYBACK_DIR = 'controller_playback/'
if not os.path.exists(os.path.dirname(BATTLE_FRAMES_dir)):
    os.makedirs(os.path.dirname(BATTLE_FRAMES_dir))

def demo_print(message):
    if not SHOWING_DEMO:
        print(message)

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

NEUTRAL_CONTROLLER = "128,128,128,128,0,0,0,0,0,0,0,0,0,0,0,0"

class RunnerApp():
    def __init__(self, serial_port, user_id, description, crop_rect=None, testing_override=False, training_override=False):
        self.twitch = None
        self.selectedAction = None
        self.localFrame = None
        self.localMessageFrame = None
        self.waitingForAction = False
        self.running_status = STOPPED
        self.user_id = user_id
        self.session_id = None
        self.service_base_url = SERVER_URL
        self.model_base_url = "https://www.genericterraformtesterurl.com"
        self.honorable_salad_base_url = MODEL_SERVER_URL

        self.runSession = False
        self.runInference = False
        self.controllerOutputStyle = runner_config['controller_output']
        self.game_name = "streets4"
        self.description = description


        self.availableActions = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.observationSpace = []
        self.transcript = ""


        self.twitchIsConnected = False
        self.sshIsConnected = False

        self.shouldRunControllerThread = False


        self.sessionType = None;
        self.performingNetworkRequest = False
        self.wait = 0.2
        self.serial = Serial(serial_port)

        # For new session
        self.crop_rect = crop_rect
        self.testing_override = testing_override
        self.training_override = training_override

        self.last_controller_1_state = '128,128,128,128,0,0,0,1,0,0,0,0,0,0,0,0'
        self.last_controller_2_state = '128,128,128,128,0,0,0,1,0,0,0,0,0,0,0,0'

        self.is_recording_controller = False
        self.is_expecting_recording = False
        self.last_recording_time = 0
        self.controller_sequences = []
        self.controller_playback_count = 0
        self.controller_playback_base_filename = str(uuid.uuid4())


    def startPauseSessionToggle(self):
        print('startPauseSessionToggle')
        if self.running_status == STOPPED:
            # Fetch Session ID
            self.createNewSession()

        if self.running_status == RUNNING:
            self.running_status = PAUSED
            self.runSession = False

        if self.running_status == PAUSED:
            self.running_status = RUNNING
            self.runSession = True

    def startControllerRecordToggle(self):
        print('startControllerRecordToggle')
        if self.is_recording_controller == True:
            self.is_recording_controller = False
            self.save_recorded_controller()
            return

        if self.is_expecting_recording == False:
            self.is_expecting_recording = True
            return

        if self.is_expecting_recording == True:
            self.is_expecting_recording = False
            return

    def save_recorded_controller(self):
        self.controller_playback_count += 1
        filename = '%scontroller_playback_%d_%s.csv' % (CONTROLLER_PLAYBACK_DIR, self.controller_playback_count, self.controller_playback_base_filename)

        arr = np.array(self.controller_sequences)

        # convert array into dataframe
        DF = pd.DataFrame(arr)

        # save the dataframe as a csv file
        DF.to_csv(filename)

        print('controller replay saved in %s' % filename)
        self.controller_sequences = []
        self.last_recording_time = 0



    def endSession(self):
        if self.session_id == None:
            print("You need to start a session before ending");
            return

        self.running_status = STOPPED
        self.runSession = False
        self.startSessionEnabled = True
        self.pauseResumeSessionEnabled = False
        self.stopSessionEnabled = False
        self.abandonSessionEnabled = False

        end_session(self.user_id, self.game_name, self.session_id, [], self.description)

    def getRunningStatusAsString(self):
        if self.running_status == STOPPED:
            return 'Not Started'

        if self.running_status == RUNNING:
            return 'Running'

        if self.running_status == PAUSED:
            return 'Paused'

    def toggleSession(self):
        self.runSession = not self.runSession

    def toggleInference(self):
        self.runInference = not self.runInference

    def handleHonableAction(self):
        slim_data = {
          "obs": self.observationSpace,
          "valid_moves": self.availableActions,
          "transcript": self.transcript,
        }

        url = self.honorable_salad_base_url + '/api/predict_basic'
        headers = {'content-type': 'application/json'}

        print('handleHonableAction url:', url)

        data_json = json.dumps(slim_data)
        response = requests.post(url, data=data_json, headers=headers)
        res = json.loads(response.text)
        print('handleHonableAction data:', res)
        action = res['action']
        self.swordActionSelected(action)


    def createNewSession(self):
        if self.session_id is not None:
            print("You need to end a session before beginning a new one., session undefined");
            return

        if self.running_status != STOPPED:
            print("You need to end a session before beginning a new one., running status not stopped");
            return

        self.session_id = start_session(self.user_id, self.crop_rect, self.testing_override, self.training_override)
        self.running_status = RUNNING
        self.runSession = True

    def get_required_fields(self):
        headers = {'content-type': 'application/json'}
        url = self.service_base_url + '/api/required_game_fields'
        print('capture url:', url)

        data_json = json.dumps(slim_data, cls=NumpyEncoder)
        response = requests.post(url, headers=headers)
#        response = requests.post(url, files=file, data=data_json, headers=headers)
        res = json.loads(response.text)

    # Update controller regularly
    def runControllerThread(self):
        self.shouldRunControllerThread = True

        while self.shouldRunControllerThread:
            if not self.runInference:
                self.writeControllerData()

            fps = 25
            time.sleep(1./fps)

    def preprocessControllerForConsole(self, controller_str):
        new_controller = controller_str.split(',')
        if self.controllerOutputStyle == "switch":
            #A,B,X,Y
            # XBOX needs A and B switched and X and Y switched
            # hold A
            holder = new_controller[8]
            #place B in A
            new_controller[8] = new_controller[9]
            # place holder in B
            new_controller[9] = holder

            # hold X
            holder = new_controller[10]
            #place Y in X
            new_controller[10] = new_controller[11]
            # place holder in Y
            new_controller[11] = holder

        if self.controllerOutputStyle == "ps4":
            #override right dpad ps4 workaround
            if int(new_controller[7]) == 1:
                new_controller[0] = '255'
                new_controller[7] = '0'
            #A,B,X,Y
            # XBOX needs A and B switched and X and Y switched
            # hold A
            holder = new_controller[8]
            #place B in A
            new_controller[8] = new_controller[9]
            # place holder in B
            new_controller[9] = holder

            # hold X
            holder = new_controller[10]
            #place Y in X
            new_controller[10] = new_controller[11]
            # place holder in Y
            new_controller[11] = holder

        if self.controllerOutputStyle == "ps4":
            #override right dpad ps4 workaround
            if int(new_controller[7]) == 1:
                new_controller[0] = '255'
                new_controller[7] = '0'
            #A,B,X,Y
            # XBOX needs A and B switched and X and Y switched
            # hold A
            holder = new_controller[8]
            #place B in A
            new_controller[8] = new_controller[9]
            # place holder in B
            new_controller[9] = holder

            # hold X
            holder = new_controller[10]
            #place Y in X
            new_controller[10] = new_controller[11]
            # place holder in Y
            new_controller[11] = holder

        return ','.join(new_controller)

    def writeControllerData(self):
        currentController = get_360_controller_state();
#        print('get_360_controller_state', currentController)
        # recordings should use raw controller before preprocessed

        if self.is_expecting_recording and currentController != NEUTRAL_CONTROLLER:
            self.is_recording_controller = True
            self.is_expecting_recording = False
            self.last_recording_time = 0
            print('Begin capture from controller')

        if self.is_recording_controller:
            delta = 0
            if self.last_recording_time != 0:
                delta = time.time() - self.last_recording_time
            self.last_recording_time = time.time()
            self.controller_sequences.append([currentController, delta])

        currentController = self.preprocessControllerForConsole(currentController)
#        print('writing', currentController)
        self.serial.send(currentController)

    def capture(self, frame):

        game_categories = []
        game_values = []
        user_categories = []
        user_values = []

        controller_info_1 = get_360_controller_state();
        controller_info_2 = get_360_controller_state();
        timestamp = time.time()

        state_dump=process_stateless(self.user_id, self.session_id, self.game_name, frame, controller_info_1, controller_info_2, timestamp, game_categories, game_values, user_categories, user_values)



    def inference(self, frame):
        ret, buffer = cv2.imencode('.jpg', frame)
        imageSrc = "data:image/jpeg;base64,"+base64.b64encode(buffer).decode()

        timestamp = time.time()

        controller_index = 0
        controller_frame_0 = update_controller_for_session(self.user_id, self.session_id, self.last_controller_1_state, timestamp, controller_index)
        controller_frame_0 = '0,'+ controller_frame_0
        controller_index = 1
        controller_frame_1 = update_controller_for_session(self.user_id, self.session_id, self.last_controller_2_state, timestamp, controller_index)
        controller_frame_1 = '1,'+ controller_frame_1

        predicted_controller = get_model_v1_prediction(frame, controller_frame_0)
        predicted_controller = predicted_controller.replace('_',',')

        controller = self.preprocessControllerForConsole(predicted_controller)
        print('preprocessControllerForConsole',controller)
        self.serial.send(controller)
        self.last_controller_1_state = predicted_controller
        print('last_controller_1_state',self.last_controller_1_state)


width = 1920
height = 1080

gst_str = ('nvarguscamerasrc ! ' + 'video/x-raw(memory:NVMM), ' +
          'width=(int)1920, height=(int)1080, ' +
          'format=(string)NV12, framerate=(fraction)30/1 ! ' +
          'nvvidconv flip-method=2 ! ' +
          'video/x-raw, width=(int){}, height=(int){}, ' +
          'format=(string)BGRx ! ' +
          'videoconvert ! appsink').format(width, height)

#cap = cv.VideoCapture(gst_str, cv.CAP_GSTREAMER)

def open_cam_usb(dev, width, height):
    # We want to set width and height here, otherwise we could just do:
    #     return cv2.VideoCapture(dev)
    gst_str = ("v4l2src device={} ! "
               "video/x-raw, width=(int){}, height=(int){}, format=(string)RGB ! "
               "videoconvert ! appsink").format(dev, width, height)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

def open_cam_usb(dev):
    # We want to set width and height here, otherwise we could just do:
    #     return cv2.VideoCapture(dev)
    gst_str = ("v4l2src device=/dev/video{} ! "
	       "image/jpeg, format=(string)RGGB ! "
	       " jpegdec ! videoconvert ! queue ! appsink").format(dev)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

PRESSED_KEY = None

def start_cycle():

    crop_rect= None
    testing_override = False
    training_override = False

    use_gstream = False
    use_twitch = False
    serial_port = runner_config['serial_port']
    device = runner_config['device']
    description = None
    user_id = "No one"
    runner = RunnerApp(serial_port, user_id, description, crop_rect, testing_override, training_override)

    action = -1

    team_label_id = None
    session_mode = None
    battle_style = None

    window_name = "Collecting Replay Data"


    #    if use_twitch:
#   runner.connectToTwitch()

#    cam = cv2.VideoCapture("/dev/video2")
#    cam = open_cam_usb("2")
    if use_gstream:
        cam = open_cam_usb(device)
    else:
        cam = cv2.VideoCapture(device)
#    cam.set(cv2.CAP_FFMPEG,True)
#    cam.set(cv2.CAP_PROP_FPS,30)
#    cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
#    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
    cam.set(cv2.CAP_PROP_BUFFERSIZE, 1);

    # CONTROLLER INPUT PROMPT BEGIN
    import numpy as np

    # Create a black image
    img = np.zeros((512,512,3), np.uint8)

    # Write some Text

    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (10,500)
    fontScale              = 1
    fontColor              = (255,255,255)
    lineType               = 2

    cv2.putText(img,'Exists to capture keyboard overrides!',
                bottomLeftCornerOfText,
                font,
                fontScale,
                fontColor,
                lineType)

    #Display the image
    cv2.imshow(window_name,img)
    # CONTROLLER INPUT PROMPT END
    i = 0
    actionCoolDown = time.time()
    wait_threshold = 0.2
    action = None
    threading.Thread(target=runner.runControllerThread).start()



    frame_count = 0

    frame_rate = runner_config['runner_max_fps']
    prev = 0

    while(True):


        cam.grab()
        success, image = cam.read()

        # Capture frame-by-frame
        time_elapsed = time.time() - prev

        if time_elapsed > 1./frame_rate:
            prev = time.time()
        else:
            continue

        height , width , layers =  image.shape
        new_h=720
        new_w=1280
        imageSrc = cv2.resize(image, (new_w, new_h))

        if runner.runSession:
            runner.capture(image)

        if runner.runInference:
            runner.inference(image)

#            time.sleep(0.4)


        # ESC key pressed
        raw_key = cv2.waitKey(delay=1)
        key = chr(raw_key & 255) if raw_key >= 0 else None
        if key is not None and PRESSED_KEY is None:
            print('key: %s raw_key: %s' % (key, raw_key))
            if raw_key in MANUAL_BUTTONS_KEY_PRESS:
                runner.serial.send(MANUAL_BUTTONS_KEY_PRESS[raw_key])
        #Abandon Session
        if raw_key == ESCAPE_KEY:
            print('Session Abandoned')
            break
        #Force ENd Session
        elif raw_key == P_KEY:
            print('Session Force Ended')
            try:
                runner.shouldRunControllerThread = False
                cam.release()
                cv2.destroyAllWindows()
            except:
                pass
            time.sleep(0.01)
            runner.endSession()
            break
        elif raw_key == BUTTON_A:
            print('Toggle Session')
            runner.toggleSession()
            time.sleep(0.1)
        elif raw_key == BUTTON_B:
            print('Toggle inference')
            runner.toggleInference()
            time.sleep(0.1)
        elif raw_key == BUTTON_Y:
            print('Toggle inference')
            runner.startPauseSessionToggle()
            time.sleep(0.1)
        elif raw_key == BUTTON_R:
            print('Toggle Controller record')
            runner.startControllerRecordToggle()
            time.sleep(0.1)

        PRESSED_KEY = key

    try:
        runner.shouldRunControllerThread = False
        cam.release()
    #    _out.release()
        cv2.destroyAllWindows()
    except:
        pass


def main(argv):

    start_cycle()



if __name__ == "__main__":
    main(sys.argv[1:])

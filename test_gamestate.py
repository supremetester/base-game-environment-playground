from centroidtracker import *
from trackableobject import *
import time, schedule, csv
import numpy as np
import argparse, imutils
import time, dlib, cv2, datetime
from itertools import zip_longest

from yolo_v3.yolov3_manager import *

import numpy as np
import cv2
import os
import uuid
import threading

def open_cam_usb(dev):
    # We want to set width and height here, otherwise we could just do:
    #     return cv2.VideoCapture(dev)
    gst_str = ("v4l2src device=/dev/video{} ! "
	       "image/jpeg, format=(string)RGGB ! "
	       " jpegdec ! videoconvert ! queue ! appsink").format(dev)
    return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

cap = cv2.VideoCapture(1) # check this

#cap = open_cam_usb("2")

gst_str = ("v4l2src device=/dev/video2 ! "
       "image/jpeg, format=(string)RGGB ! "
       " jpegdec ! videoconvert ! queue ! appsink")
#cap=cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
GAME_NAME = os.environ.get('GAME_NAME', "Arcana_Heart/")
SHOW_PREVIEW = bool(int(os.environ.get('SHOW_PREVIEW', 0)))
THREADED = bool(int(os.environ.get('THREADED', 0)))


filename_base = str(uuid.uuid4())

if not os.path.exists(os.path.dirname(GAME_NAME+'/')):
    os.makedirs(os.path.dirname(GAME_NAME+'/'))

#https://vimeo.com/549508797/64ddaf8802

import sys, getopt
import os
import numpy as np



def run():
    useVideoFile = False
    videoFileName = None
    WEB_CAM_DEV = 1

    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", type=str,
        help="path to optional input video file")
    ap.add_argument("-o", "--output", type=str,
        help="path to optional output video file")
    # confidence default 0.4
    ap.add_argument("-s", "--skip-frames", type=int, default=10,
        help="# of skip frames between detections")
    args = vars(ap.parse_args())



    window_name = str(uuid.uuid4())

    if not args.get("input", False):
        cap = cv2.VideoCapture(WEB_CAM_DEV) # check this
    else:
        cap = cv2.VideoCapture(args["input"])



    tracker_pairs = []
    tracker_labels = []
#    trackableObjects = {}

    # initialize the total number of frames processed thus far, along
    # with the total number of objects that have moved either up or down
    totalFrames = 0
    totalDown = 0
    totalUp = 0
    x = []
    empty=[]
    empty1=[]

    frame_rate = 30
    prev = 0
    writer = None


    # loop over frames from the video stream
    while True:

        time_elapsed = time.time() - prev
        res, image = cap.read()

        if time_elapsed > 1./frame_rate:
            prev = time.time()
        else:
            continue

        # grab the next frame and handle if we are reading from either
        # VideoCapture or VideoStream
        # Capture frame-by-frame
        ret, frame = cap.read()

        height , width , layers =  frame.shape
        H=new_h=720
        W=new_w=1280
        imageSrc = cv2.resize(frame, (new_w, new_h))

        frame = cap.read()
        frame = frame[1] if args.get("input", False) else frame

        # if we are viewing a video and we did not grab a frame then we
        # have reached the end of the video
        if args["input"] is not None and frame is None:
            break

        # resize the frame to have a maximum width of 500 pixels (the
        # less data we have, the faster we can process it), then convert
        # the frame from BGR to RGB for dlib
        frame = imutils.resize(frame, width = 500)
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # if we are supposed to be writing a video to disk, initialize
        # the writer
        if args["output"] is not None and writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            writer = cv2.VideoWriter(args["output"], fourcc, 30,
                (new_w, new_h), True)

        # if the video writer is not None, write the frame to the output video file
        if writer is not None:
            writer.write(frame)

        # initialize the current status along with our list of bounding
        # box rectangles returned by either (1) our object detector or
        # (2) the correlation trackers
        status = "Waiting"
        rects = []

        # check to see if we should run a more computationally expensive
        # object detection method to aid our tracker
        if totalFrames % args["skip_frames"] == 0:
            # set the status and initialize our new set of object trackers
            status = "Detecting"
            tracker_pairs = []

            # convert the frame to a blob and pass the blob through the
            # network and obtain the detections
            labels_and_boxes = predict_image(frame)

            # loop over the detections
            for label_key in labels_and_boxes:
                for box in labels_and_boxes[label_key]:
                    # construct a dlib rectangle object from the bounding
                    # box coordinates and then start the dlib correlation
                    # tracker
                    tracker = dlib.correlation_tracker()
                    startX, startY, endX, endY = box
                    rect = dlib.rectangle(startX, startY, endX, endY)
                    tracker.start_track(rgb, rect)

                    # add the tracker to our list of trackers so we can
                    # utilize it during skip frames
                    tracker_pairs.append([label_key, tracker])

        # otherwise, we should utilize our object *trackers* rather than
        # object *detectors* to obtain a higher frame processing throughput
        else:
            labels_and_boxes = {}
            # loop over the trackers
            for tracker_pair in trackers:
                # set the status of our system to be 'tracking' rather
                # than 'waiting' or 'detecting'
                status = "Tracking"

                # update the tracker and grab the updated position
                tracker.update(rgb)
                pos = tracker_pair[1].get_position()
                label = tracker_pair[0]
                if label not in labels_and_boxes:
                    labels_and_boxes[label] = []

                # unpack the position object
                startX = int(pos.left())
                startY = int(pos.top())
                endX = int(pos.right())
                endY = int(pos.bottom())

                # add the bounding box coordinates to the rectangles list
                labels_and_boxes[label].append((startX, startY, endX, endY))

        # draw a horizontal line in the center of the frame -- once an
        # object crosses this line we will determine whether they were
        # moving 'up' or 'down'
        cv2.line(frame, (0, H // 2), (W, H // 2), (0, 0, 0), 3)
#        cv2.putText(frame, "-Prediction border - Entrance-", (10, H - ((i * 20) + 200)),
#            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)

        # use the centroid tracker to associate the (1) old object
        # centroids with (2) the newly computed object centroids
        objects = ct.update(labels_and_boxes)

        # preregister for callbacks
        objects, labels, objectCountIDs = ct.get_advance_states()

        # loop over the tracked objects
        for (objectID, centroid) in objects.items():
            # check to see if a trackable object exists for the current
            # object ID
            to = trackableObjects.get(objectID, None)

            # if there is no existing trackable object, create one
            if to is None:
            	to = TrackableObject(objectID, centroid, labels[objectID])

            # store the trackable object in our dictionary
            trackableObjects[objectID] = to

        # register new objects
        ct.process_detections()


        objects, labels, objectCountIDs = ct.get_advance_states()

        # loop over the tracked objects
        for (objectID, centroid) in objects.items():
            # check to see if a trackable object exists for the current
            # object ID
            to = trackableObjects.get(objectID, None)

            # if there is no existing trackable object, create one
            if to is None:
            	to = TrackableObject(objectID, centroid, labels[objectID])

            # otherwise, there is a trackable object so we can utilize it
            # to determine direction
            else:
                # the difference between the y-coordinate of the *current*
                # centroid and the mean of *previous* centroids will tell
                # us in which direction the object is moving (negative for
                # 'up' and positive for 'down')
                y = [c[1] for c in to.centroids]
                direction = centroid[1] - np.mean(y)
                to.centroids.append(centroid)

                # check to see if the object has been counted or not
                if not to.counted:
                    # if the direction is negative (indicating the object
                    # is moving up) AND the centroid is above the center
                    # line, count the object
                    if direction < 0 and centroid[1] < H // 2:
                        totalUp += 1
                        empty.append(totalUp)
                        to.counted = True

                    # if the direction is positive (indicating the object
                    # is moving down) AND the centroid is below the
                    # center line, count the object
                    elif direction > 0 and centroid[1] > H // 2:
                        totalDown += 1
                        empty1.append(totalDown)
                        #print(empty1[-1])
                        x = []
                        # compute the sum of total people inside
                        x.append(len(empty1)-len(empty))
                        #print("Total people inside:", x)
                        # if the people limit exceeds over threshold, send an email alert
                        if sum(x) >= config.Threshold:
                            cv2.putText(frame, "-ALERT: People limit exceeded-", (10, frame.shape[0] - 80),
                                cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 2)
                            if config.ALERT:
                                print("[INFO] Sending email alert..")
                                Mailer().send(config.MAIL)
                                print("[INFO] Alert sent")

                        to.counted = True


            # store the trackable object in our dictionary
            trackableObjects[objectID] = to

            # draw both the ID of the object and the centroid of the
            # object on the output frame
            text = "ID {}".format(objectID)
            cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            cv2.circle(frame, (centroid[0], centroid[1]), 4, (255, 255, 255), -1)

        # construct a tuple of information we will be displaying on the
        info = [
        ("Exit", totalUp),
        ("Enter", totalDown),
        ("Status", status),
        ]

        info2 = [
        ("Total people inside", x),
        ]

        projectile_owner_message  = ""

        for idx, objectID in enumerate(trackableObjects):
            ownedIds = ct.get_objects_for_owner(objectID)
            if len(ownedIds) == 0:
                continue

            projectile_owner_message = '%s has %d projectiles on screen.' % (trackableObjects[objectID].label, len(ownedIds))
            print(projectile_owner_message)
            cv2.putText(frame, projectile_owner_message, (80, ((i * 20) + 90)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 2)


                # Display the output
        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 2)

        for (i, (k, v)) in enumerate(info2):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (265, H - ((i * 20) + 60)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)



        # show the output frame
        cv2.imshow("Real-Time Monitoring/Analysis Window", frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

        # increment the total number of frames processed thus far and
        # then update the FPS counter


        if SHOW_PREVIEW:
            # Our operations on the frame come here
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Display the resulting frame
            cv2.imshow('frame',frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # stop the timer and display FPS information
#    print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
#    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))


    # # if we are not using a video file, stop the camera video stream
    if not args.get("input", False):
        cap.stop()
    #
    # # otherwise, release the video file pointer
    else:
        cap.release()

    # close any open windows
    cv2.destroyAllWindows()


if __name__ == "__main__":
#    main(sys.argv[1:])

    run()
